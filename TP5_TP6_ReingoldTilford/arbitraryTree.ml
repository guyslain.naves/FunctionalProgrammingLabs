open Bintree
module Option = Core.Option

let rec print (elt_print : 'elt QCheck.Print.t) : 'elt t QCheck.Print.t =
  function
  | Leaf -> "Leaf"
  | Node (left,root,right) ->
    Printf.sprintf "Node (%s, %s, %s)"
      (print elt_print left)
      (elt_print root)
      (print elt_print right)


let rec shrink (elt_shrink : 'elt QCheck.Shrink.t) : 'elt t QCheck.Shrink.t =
  let open QCheck.Iter in
  function
  | Leaf -> empty
  | Node (left,root,right) ->
    of_list [ left; right]
    <+> (shrink elt_shrink left >|= fun left_red ->
         Node (left_red,root,right))
    <+> (shrink elt_shrink right >|= fun right_red ->
         Node (left,root,right_red))
    <+> (elt_shrink root >|= fun root_red ->
         Node (left,root_red,right))

let rec gen_tree (elt_gen : 'elt QCheck.Gen.t) (size : int) : 'elt t QCheck.Gen.t =
  let open QCheck.Gen in 
  if size <= 0 then return Leaf
  else
    0 -- (size-1) >>= fun size_left ->
    let size_right = size - size_left - 1 in
    gen_tree elt_gen size_left >>= fun left ->
    gen_tree elt_gen size_right >>= fun right ->
    elt_gen >>= fun root ->
    return (Node (left,root,right))


let tree (elt_arbitrary : 'elt QCheck.arbitrary) :
  'elt t QCheck.arbitrary =
  let open QCheck in 
  QCheck.make
    ?print:Option.(elt_arbitrary.print >>| print)
    ?shrink:Option.(elt_arbitrary.shrink >>| shrink)
    QCheck.Gen.(sized (gen_tree elt_arbitrary.gen))

let int_tree = tree QCheck.small_int
let unit_tree = tree QCheck.unit 
