type 'elt t =
  | Leaf
  | Node of 'elt t * 'elt * 'elt t

val single : 'elt -> 'elt t 

val complete : int -> unit t
val left_path : int -> unit t
val right_path : int -> unit t
val left_caterpillar : int -> unit t
val right_caterpillar : int -> unit t

val height : 'elt t -> int 
val mirror : 'elt t -> 'elt t

val map : 'elt t -> f:('elt -> 'im) -> 'im t
val fold : f:('state -> 'elt -> 'state) -> init:'state -> 'elt t -> 'state

val fold_map :
  fold:('state -> 'im -> 'state) ->
  map:('elt -> 'im) ->
  init:'state -> 'elt t -> 'state
  
val cardinal : 'elt t -> int

val to_node_list : 'elt t -> 'elt list
val to_edge_list : 'elt t -> ('elt * 'elt) list

val to_image : Gg.p2 t -> Vg.image

val find_trivial_embedding : 'elt t -> ('elt * Gg.p2) t 
