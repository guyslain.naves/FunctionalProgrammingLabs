open Bintree 

let special_trees_have_correct_depth d =
  List.for_all
    (fun gen_tree ->  height (gen_tree d) = d)
    [ complete;
      left_path;
      right_path;
      left_caterpillar;
      right_caterpillar
    ]

let test_height_complete =
  QCheck.Test.make
    ~count:10
    QCheck.((-1) -- 15)
    special_trees_have_correct_depth


let mirror_is_involutive tree =
  mirror (mirror tree) = tree

let test_mirror_involutive =
  QCheck.Test.make
    ArbitraryTree.unit_tree
    mirror_is_involutive


let left_and_right_paths_are_mirror d =
  mirror (left_path d) = right_path d
    
let test_path_mirror =
  QCheck.Test.make
    QCheck.((-1) -- 100)
    left_and_right_paths_are_mirror
    
let left_and_right_caterpillars_are_mirror d =
  mirror (left_caterpillar d) = right_caterpillar d

let test_caterpillar_mirror =
  QCheck.Test.make
    QCheck.((-1) -- 100)
    left_and_right_caterpillars_are_mirror


let map_preserves_height (tree,f) =
  height (map ~f tree) = height tree

let test_map_height =
  QCheck.Test.make
    QCheck.(pair ArbitraryTree.int_tree (fun1 int int))
    map_preserves_height

let map_and_id_law tree =
  tree = map tree ~f:(fun x -> x)
    
let test_map_id =
  QCheck.Test.make
    ArbitraryTree.int_tree
    map_and_id_law

let map_and_compose_law (tree,f,g) =
  (tree |> map ~f |> map ~f:g) = (tree |> map ~f:(fun x -> g (f x)))

let test_map_compose =
  QCheck.Test.make
    QCheck.(triple ArbitraryTree.int_tree (fun1 int int) (fun1 int int))
    map_and_compose_law

let map_and_fold_law (tree,init,mapped,folded) =
  (tree |> map ~f:mapped |> fold ~f:folded ~init)
  = (tree |> fold ~f:(fun state elt -> folded state (mapped elt)) ~init)

let test_map_fold =
  QCheck.Test.make
    QCheck.(quad
              ArbitraryTree.int_tree
              char
              (fun1 int int)
              (fun2 char int char)
           )
    map_and_fold_law


let nodes_and_edges_numbers_differ_by_one tree =
  tree = Leaf ||
  List.length (to_node_list tree) = List.length (to_edge_list tree) + 1

let test_nodes_edges_numbers =
  QCheck.Test.make
    ArbitraryTree.int_tree
    nodes_and_edges_numbers_differ_by_one


let rec power a exp =
  if exp = 0 then 1
  else if exp mod 2 = 0 then power (a*a) (exp/2)
  else a * power (a*a) (exp/2)
         
let complete_has_almost_power_of_two_nodes d =
  List.length (to_node_list (complete d)) = power 2 (d+1) - 1

let test_complete_nodes =
  QCheck.Test.make
    QCheck.((-1) -- 15)
    complete_has_almost_power_of_two_nodes

let caterpillar_has_two_d_nodes d =
  let nbr_nodes = List.length (to_node_list (left_caterpillar d)) in 
  if d = -1 then nbr_nodes = 0
  else if d = 0 then nbr_nodes = 1
  else nbr_nodes = 2 * d

let test_caterpillar_nodes =
  QCheck.Test.make
    QCheck.(1 -- 1000)
    caterpillar_has_two_d_nodes


let fold_nodes_is_same_as_fold_list (tree,init,f) =
  fold ~f ~init tree =
  List.fold_left f init (to_node_list tree)

let test_fold_tree_or_list =
  QCheck.Test.make
    QCheck.(triple ArbitraryTree.int_tree char (fun2 char int char))
    fold_nodes_is_same_as_fold_list
    

let run_tests =
  List.iter
    QCheck.Test.check_exn
    [ test_height_complete;
      test_mirror_involutive;
      test_path_mirror;
      test_caterpillar_mirror;
      test_map_height;
      test_map_compose;
      test_map_id;
      test_map_fold;
      test_nodes_edges_numbers;
      test_complete_nodes;
      test_caterpillar_nodes;
      test_fold_tree_or_list
    ]
