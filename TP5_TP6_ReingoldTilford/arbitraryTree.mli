val tree : 'elt QCheck.arbitrary -> 'elt Bintree.t QCheck.arbitrary

val int_tree : int Bintree.t QCheck.arbitrary
val unit_tree : unit Bintree.t QCheck.arbitrary
