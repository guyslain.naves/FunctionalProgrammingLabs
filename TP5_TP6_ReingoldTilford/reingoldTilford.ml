module Option = Core.Option

type levels =
  | Empty
  | Interval of (Interval.t * levels)
  | Merge of levels * levels
  | Shift of float * levels


let merge_levels (inter1,levels1) (inter2,levels2) =
  ( Interval.merge inter1 inter2,
    Merge (levels1, levels2)
  )

let shift_levels ~delta (inter,levels) =
  (Interval.shift delta inter, Shift (delta, levels))

let rec observe_levels = function
  | Empty ->
    None
  | Interval (interval,levels) ->
    Some (interval,levels)
  | Shift (delta1, Shift (delta2, levels)) ->
    observe_levels (Shift (delta1 +. delta2, levels))
  | Shift (delta, levels) ->
    Option.map ~f:(shift_levels ~delta)
      (observe_levels levels)
  | Merge (levels1, levels2) ->
    Option.merge ~f:merge_levels
      (observe_levels levels1)
      (observe_levels levels2)



let compute_necessary_gap left_levels right_levels = 
  let rec recurse left_levels right_levels maxi k =
    match observe_levels left_levels, observe_levels right_levels with
    | None, None ->
      k maxi Empty
    | None, Some (right,levels) ->
      k maxi (Shift (maxi /. 2., Interval (right,levels)))
    | Some (left,levels), None ->
      k maxi (Shift (-. maxi /. 2., Interval (left,levels)))
    | Some (left,left_levels), Some (right,right_levels) ->
      let local_gap =
        2. -. (Interval.gap left right |> Option.value ~default:0.)
      in
      let current_gap = max maxi local_gap in 
      recurse left_levels right_levels current_gap @@ fun gap lower_levels ->
      let sleft = Interval.shift (-. gap /. 2.) left in
      let sright = Interval.shift (gap /. 2.) right in 
      Interval (Interval.merge sleft sright, lower_levels)
	    |> k gap                     
  in
  recurse left_levels right_levels 2. @@ fun gap levels ->
  (gap, levels)
        

let rec annotate_with_offset =
  let open Bintree in
  function
  | Leaf -> (Leaf, Empty)
  | Node (left,root,right) ->
    let (left, left_levels) = annotate_with_offset left in
    let (right, right_levels) = annotate_with_offset right in
    let (gap, merged_levels) =
      compute_necessary_gap left_levels right_levels
    in
    ( Node (left, (root, gap /. 2.), right),
      Interval (Interval.v 0. 0., merged_levels)
    )


let rec annotate_with_position ~at =
  let open Bintree in
  function
  | Leaf -> Leaf
  | Node (left, (root,shift), right) ->
    Node (
      annotate_with_position ~at:Gg.V2.(at + v shift (-1.)) left,
      (root, at),
      annotate_with_position ~at:Gg.V2.(at + v (-. shift) (-1.)) right
    )

let layout tree =
  tree
  |> annotate_with_offset
  |> fst
  |> annotate_with_position ~at:Gg.P2.o



    
let op_by_coord op vec1 vec2 =
  Gg.V2.(v (op (x vec1) (x vec2)) (op (y vec1) (y vec2)))

let min_coord = op_by_coord min
let max_coord = op_by_coord max


let compute_view = function
  | Bintree.Leaf -> Gg.(Box2.v P2.(v 0. 0.) V2.(v 1. 1.))
  | (Bintree.Node (_,pos_root,_)) as tree ->
    let open Bintree in 
    let min_vec = Bintree.fold ~f:min_coord ~init:pos_root tree in
    let max_vec = Bintree.fold ~f:max_coord ~init:pos_root tree in 
    Gg.(Box2.v
          V2.(min_vec - v 1. 1.)
          V2.(max_vec - min_vec + v 2. 2.)
       )
