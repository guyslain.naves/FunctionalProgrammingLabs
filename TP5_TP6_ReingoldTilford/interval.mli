type t 

val empty : t
val v : float -> float -> t
val (--) : float -> float -> t

val get_lower : t -> float option
val get_upper : t -> float option 

val shift : float -> t -> t

val merge : t -> t -> t

val gap : t -> t -> float option 
