module StdLibRandom = Random
  
open Core


type 'elt t =
  | Leaf
  | Node of 'elt t * 'elt * 'elt t



let single elt = Node (Leaf, elt, Leaf)
    
let rec complete depth =
  if depth = -1 then
    Leaf
  else
    let subtree = complete (depth-1) in 
    Node (subtree,(),subtree)

let rec left_path depth =
  if depth = -1 then Leaf
  else Node (left_path (depth-1), (), Leaf)

let rec right_path depth =
  if depth = -1 then Leaf
  else Node (Leaf, (), right_path (depth-1))

let rec left_caterpillar depth =
  if depth = -1 then Leaf
  else if depth = 0 then single ()
  else if depth = 1 then Node (Leaf,(),single ())
  else Node (left_caterpillar (depth-1), (), single ())

let rec right_caterpillar depth =
  if depth = -1 then Leaf
  else if depth = 0 then single ()
  else if depth = 1 then Node (single (), (), Leaf)
  else Node (single (), (), right_caterpillar (depth-1))


let rec height = function
  | Leaf -> -1
  | Node (left,root,right) -> 1 + max (height left) (height right)



let rec mirror = function
  | Leaf -> Leaf
  | Node (left,root,right) -> Node (mirror right, root, mirror left)


let rec map tree ~f =
  match tree with
  | Leaf -> Leaf
  | Node (left,root,right) -> Node (map left ~f, f root, map right ~f)


let rec fold ~f ~init = function
  | Leaf -> init
  | Node (left, root, right) ->
    fold ~f ~init left |> fun res_left ->
    f res_left root |> fun res_root ->
    fold ~f ~init:res_root right

let fold_map ~fold:f ~map:m ~init tree =
  tree
  |> map ~f:m
  |> fold ~f:f ~init 

let rec cardinal = function
  | Leaf -> 0
  | Node (left,_,right) -> cardinal left + 1 + cardinal right

let rec to_node_list = function
  | Leaf -> []
  | Node (left,root,right) ->
    to_node_list left
    @ [root]
    @ to_node_list right


let edge_with parent = function
  | Leaf -> []
  | Node (_,child,_) -> [ (parent,child) ]
                        
let rec to_edge_list = function
  | Leaf -> []
  | Node (left, root, right) ->
    to_edge_list left
    @ edge_with root left
    @ edge_with root right 
    @ to_edge_list right




let draw_node position =
  let path =
    Vg.P.empty
    |> Vg.P.circle position 0.2
  in
  Vg.I.const Gg.Color.black
  |> Vg.I.cut ~area:`Anz path


let draw_edge (pos1, pos2) =
  let path =
    Vg.P.empty
    |> Vg.P.sub pos1
    |> Vg.P.line pos2
  in
  Vg.I.const Gg.Color.black
  |> Vg.I.cut ~area:Vg.P.(`O { o with width = 0.02 }) path

let to_image tree =
  let nodes = to_node_list tree |> List.map ~f:draw_node in
  let edges = to_edge_list tree |> List.map ~f:draw_edge in
  List.fold_left
    ~f:(fun back front -> Vg.I.blend front back)
    ~init:(Vg.I.const Gg.Color.white)
    (edges @ nodes)



let find_trivial_embedding tree =
  let rec embed_root_at position = function
    | Leaf -> Leaf
    | Node (left, root, right) ->
      Node (
        embed_root_at Gg.V2.(position + v (-1.) (-1.)) left,
        (root,position),
        embed_root_at Gg.V2.(position + v 1. (-1.)) right
      )
  in
  embed_root_at Gg.V2.zero tree


