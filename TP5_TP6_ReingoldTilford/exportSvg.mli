(** Draw an image into a file in format svg *)
val draw :
  ?view:Gg.box2 ->
  title:string ->
  description:string ->
  filename:string ->
  Vg.image ->
  unit

(** draw a path in the given color (or black) into a file in format svg *)
val draw_path :
  ?view:Gg.box2 ->
  ?color:Gg.Color.t ->
  title:string ->
  description:string ->
  filename:string ->
  Vg.path -> 
  unit  
