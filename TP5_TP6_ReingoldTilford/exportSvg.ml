let img_of_path ~color path =
  let area = Vg.P.(`O {o with width = 0.05}) in
  Vg.I.const color
  |> Vg.I.cut ~area path

let size = Gg.Size2.v 200. 200.
let view =
  let open Gg in
  Box2.v V2.(v (-20.)(-38.)) V2.(v 40. 40.)

let draw ?(view=view) ~title ~description ~filename img =
  let open Vg in 
  let out_chan = open_out filename in
  let target =
    Vgr_svg.target
      ~xmp:(Vgr.xmp ~title ~description ())
      ()
  in
  let img =
    Vg.I.blend img (Vg.I.const Gg.Color.white)
  in 
  let renderer =
    Vgr.create
      ~warn:(Vgr.pp_warning Format.err_formatter)
      target 
      (`Channel out_chan)
  in
  ignore (Vgr.render renderer (`Image (size, view, img)));
  ignore (Vgr.render renderer `End);
  close_out out_chan     

let draw_path
    ?(view=view)
    ?(color=Gg.Color.black)
    ~title
    ~description
    ~filename
    path =
  let img = img_of_path ~color path in
  draw ~title ~description ~filename img 
