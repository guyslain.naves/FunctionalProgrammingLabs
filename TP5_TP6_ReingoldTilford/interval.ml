type t =
  | Empty
  | Defined of float * float

let empty = Empty

let v from upto =
  if from <= upto then Defined (from,upto)
  else empty

let (--) = v

let get_lower = function
  | Empty -> None
  | Defined (lower,_) -> Some lower
let get_upper = function
  | Empty -> None
  | Defined (_,upper) -> Some upper 

let shift delta = function
  | Empty -> Empty
  | Defined (from,upto) -> Defined (from +. delta, upto +. delta)

let merge inter1 inter2 =
  match inter1, inter2 with
  | Empty, _ -> inter2
  | _, Empty -> inter1
  | Defined (from1,upto1), Defined (from2,upto2) ->
    Defined (min from1 from2, max upto1 upto2)


let gap inter1 inter2 =
  match inter1, inter2 with
  | Empty,_ -> None
  | _, Empty -> None
  | Defined (from1,upto1), Defined (from2,upto2) ->
    Some (from2 -. upto1)

