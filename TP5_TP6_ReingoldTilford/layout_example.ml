
               
let _ = Random.self_init ()
    
let () =
  let tree =
    QCheck.(ArbitraryTree.unit_tree.gen (Random.get_state ()))
    |> ReingoldTilford.layout
    |> Bintree.map ~f:snd
  in
  tree 
  |> Bintree.to_image
  |> ExportSvg.draw
    ~view:(ReingoldTilford.compute_view tree)
    ~title:"random tree"
    ~description:"An arbitrary tree"
    ~filename:"pic/arbitrary.svg"
