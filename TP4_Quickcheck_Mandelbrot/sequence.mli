(** 
   Some functions on sequences defined by simple recursive functions,
   of the form:
   u_0 : initial value 
   u_{n+1} = f u_n
*)


(** A term of a mathematical sequence over type ['value] is given by
    an index and a value of type ['value]: (i,u_i) *)
type 'value term = (int * 'value)

(** computes the next term, given a current term and the function
    that defines the sequence. *)
val next_term : fct:('value -> 'value) -> 'value term -> 'value term


(** computes the first term of the sequence satisfying some property. *)
val first_term :
  fct:('value -> 'value) ->
  satisfying:('value term -> bool) ->
  'value term -> 'value term


val repeat : n:int -> f:('a -> 'a) -> 'a -> 'a

val repeat_until : condition:('a -> bool) -> f:('a -> 'a) -> 'a -> 'a
