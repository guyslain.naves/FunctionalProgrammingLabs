let repeat_zero_is_identity (f,value) =
  let open QCheck in
  Sequence.repeat ~n:0 ~f:(Fn.apply f) value = value

let repeat_succ_n_is_f_of_repeat_n (n,f,value) =
  let open QCheck in 
  Sequence.repeat ~n:(n+1) ~f:(Fn.apply f) value
  = Sequence.repeat ~n ~f:(Fn.apply f) (Fn.apply f value)


let repeat_until_satifies_condition (f,value,condition) =
  let open QCheck in
  Fn.apply condition (
    Sequence.repeat_until
      ~condition:(Fn.apply condition)
      ~f:(Fn.apply f)
      value
  )

let test_repeat_zero =
  let open QCheck in
  Test.make
    ~count:100
    ~max_fail:10
    ~name:"repeat 0 f = id"
    (pair (fun1 Observable.int int) int)
    repeat_zero_is_identity


let test_repeat_succ =
  let open QCheck in
  Test.make
    ~count:100
    ~max_fail:10
    ~name:"repeat (n+1) f = repeat n f . f"
    (triple small_nat (fun1 Observable.int int) int)
    repeat_succ_n_is_f_of_repeat_n


let test_repeat_until_condition =
  let open QCheck in
  Test.make
    ~count:100
    ~max_fail:10
    ~name:"condition (repeat_until condition f) = always"
    (triple (fun1 Observable.int int) int (fun1 Observable.int bool))
    repeat_until_satifies_condition
    
    
let analyse_test_result test test_result =
  let open QCheck.Test in
  let open QCheck.TestResult in
  let test_name =  get_name test in
  let arbitrary = get_arbitrary test in 
  match test_result.state with
  | Success -> Printf.printf "%s: Success.\n" test_name
  | Failed failures ->
    Printf.printf "%s: Counter examples :\n" test_name;
    List.iter (fun fail -> Printf.printf "%s\n" (print_c_ex arbitrary fail)) failures
  | Error (instance, exceptn,_) ->
    print_error arbitrary test_name (instance, exceptn)
  |> Printf.printf "Exception:\n%s"
 
let run_test (QCheck.Test.Test cell) =
  QCheck.Test.check_cell cell
  |> analyse_test_result cell
  
let run_all_test =
  print_newline ();
  List.iter run_test
    [
      test_repeat_zero;
      test_repeat_until_condition;
      test_repeat_succ;
    ]

