(** A module to create list of colors defining gradients of colors. 

    A gradient might just be given by two colors, but it may be more
    interesting to create gradients through multiple colors. A simple
    linear gradient is thus only color plus the proportion of points
    in that linear gradient. Then we can juxtapose several gradients,
    each with its proportion, in the desired order. For instance, say
    we want a gradient from red to blue to green, with green being at
    40%, we can describe it by :
    [[ (0.4,red,blue);
       (0.6,blue,green)]
    ]
*)

(** The type of description of piecewise linear gradients *)
type gradient_description =
  (float * Gg.color * Gg.color) list


(** A simple linear gradient as a list of [n] colors. *)
val linear_gradient : n:int -> Gg.color -> Gg.color -> Gg.color list 


(** [make ~size ~descr] creates a list of colors of length [size]
    following the gradient pattern given by [descr] *)
val make : size:int -> descr:gradient_description -> Gg.color list

(** An example of three-piece gradient. See the source to see how to
    define it *)
val gray_red_yellow_green : gradient_description
