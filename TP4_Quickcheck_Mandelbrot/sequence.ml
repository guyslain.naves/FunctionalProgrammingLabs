type 'value term = (int * 'value)

let next_term ~fct (i,u_i) = (i+1, fct u_i)

let rec repeat ~n ~f init =
  if n = 0 then init
  else repeat ~n:(n-1) ~f (f init)

let rec repeat_until ~condition ~f init =
  if condition init then init
  else repeat_until ~condition ~f (f init)

let first_term ~fct ~satisfying:condition init =
  repeat_until ~condition ~f:(next_term ~fct) init
