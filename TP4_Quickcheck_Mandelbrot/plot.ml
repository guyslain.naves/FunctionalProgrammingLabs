open MoreList

type pixel =
  { x : int;
    y : int;
    rel_x : float;
    rel_y : float;
  }

let x pix = pix.rel_x
let y pix = pix.rel_y
              

let points () =
  let x_max = Graphics.size_x () in
  let y_max = Graphics.size_y () in
  let fl_x_max = float (x_max - 1) in
  let fl_y_max = float (y_max - 1) in 
  range 0 (x_max - 1) >>= fun x ->
  range 0 (y_max - 1) >>= fun y ->
  [ { x; y;
      rel_x = float x /. fl_x_max;
      rel_y = float y /. fl_y_max
    } ]


let to_color ggcol =
  let (r,g,b) = (Gg.Color.r ggcol, Gg.Color.g ggcol, Gg.Color.b ggcol) in
  let to_int float = float *. 255. |> int_of_float in 
  Graphics.rgb (to_int r) (to_int g) (to_int b)
    

let draw_pixel pixel color =
  Graphics.set_color (to_color color);
  Graphics.plot pixel.x pixel.y

let draw pixels colors =
  Graphics.(set_color black);
  Graphics.(fill_rect 0 0 (size_x () - 1) (size_y () - 1));
  List.iter2 draw_pixel pixels colors
