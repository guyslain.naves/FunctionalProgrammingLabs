let box ~center ~width =
  let hw = width /. 2. in 
  Gg.Box2.v
    Gg.V2.(center - v hw hw)
    Gg.V2.(v width width)


let complex_of_pixel ~box pixel =
  Cplex.complex
    (Gg.Box2.minx box +. Plot.x pixel *. Gg.Box2.w box)
    (Gg.Box2.miny box +. Plot.y pixel *. Gg.Box2.h box)


let box0 =
  box ~center:Gg.V2.(v 0. 0.) ~width:8.
let box1 =
  box ~center:Gg.V2.(v (-0.8) 0.) ~width:4.
let box2 =
  box ~center:Gg.V2.(v (-0.8) 0.) ~width:1.
let box3 =
  box ~center:Gg.V2.(v (-0.82) (-0.19)) ~width:0.038
let box4 =
  box ~center:Gg.V2.(v 0.001643721971153 0.822467633298876) ~width:0.0000003
let box5 =
  box ~center:Gg.V2.(v (-0.743643887037151) 0.13182590420533) ~width:0.00003
let box6 =
  box ~center:Gg.V2.(v (-0.743643887037151) 0.13182590420533) ~width:0.000017
