type t

val zero : t
val one : t 
val i : t

val complex : float -> float -> t

val ( +: ) : t -> t -> t 
val ( -: ) : t -> t -> t
val negate : t -> t

val ( *: ) : t -> t -> t
val invert : t -> t
val ( /: ) : t -> t -> t

val real : t -> float
val imaginary : t -> float

val norm : t -> float
val norm2 : t -> float 

val rotate : float -> t -> t
val translate : t -> t -> t 
val scale : float -> t -> t 

val arbitrary : t QCheck.arbitrary

val to_string : t -> string
val compare : t -> t -> int
val are_almost_equal : t -> t -> bool 
