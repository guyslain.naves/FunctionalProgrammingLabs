open MoreList
    
type gradient_description =
  (float * Gg.color * Gg.color) list



let linear_gradient ~n col1 col2 =
  let float_n = float (n-1) in
  range 0 (n-1) >>= fun i ->
  let alpha = float i /. float_n in
  let beta = 1. -. alpha in 
  [ Gg.V4.(smul alpha col2 + smul beta col1) ]


let make ~size ~descr =
  let sum_of_weight = List.fold_left (fun sum (w,_,_) -> sum +. w) 0. descr in
  let rec loop length = function
    | [] -> []
    | [(_,c1,c2)] -> linear_gradient ~n:length c1 c2
      | (prop,c1,c2)::tail ->
        let n = int_of_float (prop *. float size /. sum_of_weight +. 0.5) in
        List.rev_append
          (linear_gradient ~n c2 c1)
          (loop (length - n) tail)
  in   
  loop size descr |> List.rev


let yellow =
  Gg.Color.of_srgb Gg.V4.(v 1. 1. 0. 1.)

let gray_red_yellow_green =
  [ (0.55, Gg.Color.gray 0.4, Gg.Color.gray 0.6);
    (0.35, Gg.Color.gray 0.6, Gg.Color.red);
    (0.095, Gg.Color.red, yellow);
    (0.005, yellow, Gg.Color.green)
  ]
