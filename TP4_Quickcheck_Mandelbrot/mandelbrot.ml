  
let epsilon = 1e-12

let is_in_cardioid z =
  let dist = Cplex.norm2 z in
  dist *. (8. *. dist -. 3. ) <= 3. /. 32. -. Cplex.real z -. epsilon 
                                 
let sixteenth = 1. /. 16. 
let is_in_2_bulb z =
  Cplex.(norm2 (z -: complex (-1.) 0.)) < sixteenth -. epsilon 
                                          

let polynom =
  let open Renderer in 
  { degree = 2;
    poly = Cplex.(fun c z -> z *: z +: c);
    is_in_black_area = (fun c -> is_in_2_bulb c || is_in_cardioid c)
  }




let () = Graphics.open_graph " 1200x1200"


let () =
  Renderer.draw
    ~box:View.box6
    ~polynom
    ~gradient:Histogram.gray_red_yellow_green
  
let () = ignore (Graphics.wait_next_event [Graphics.Button_down])
