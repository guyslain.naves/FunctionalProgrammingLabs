type t = float * float

let zero = (0.,0.)
let one = (1.,0.)
let i = ( 0., 1.)

let complex real imaginary = (real, imaginary)

let ( +: ) (re1,im1) (re2,im2) =
  (re1 +. re2, im1 +. im2)

let ( -: ) (re1, im1) (re2,im2) =
  (re1 -. re2, im1 -. im2)

let negate (re,im) = (-. re, -. im)

let ( *: ) (re1,im1) (re2,im2) =
  (re1 *. re2 -. im1 *. im2,
   re1 *. im2 +. re2 *. im1)

let invert (re, im) =
  let norm = re ** 2. +. im ** 2. in
  (re /. norm, -. im /. norm)

let ( /: ) c1 c2 = c1 *: invert c2


let norm2 (re,im) = re *. re +. im *. im
let norm c = sqrt (norm2 c)


let real (re,im) = re
let imaginary (re,im) = im

let rotate angle complex = (cos angle, sin angle) *: complex
let translate = ( +: )
let scale lambda (re,im) = (lambda *. re, lambda *. im)

let to_string (re,im) =
  Printf.sprintf "%f + %f i" re im 

let arbitrary = QCheck.(pair float float)

let compare (re1,im1) (re2,im2) =
  let compare_float f1 f2 =
    if abs_float (f1 -. f2) < 1e-6 then 0
    else Pervasives.compare f1 f2
  in
  let real_delta = compare_float re1 re2 in
  if real_delta = 0 then
    compare_float im1 im2
  else real_delta 

let are_almost_equal c1 c2 =
  compare c1 c2 = 0
