(** Same as [List.concat], but reverses the order of the elements. 
    Tail-recursive.
*)
val rev_concat : 'elt list list -> 'elt list


(** Usual bind, for lists. *)
val bind : ('elt -> 'im list) -> 'elt list -> 'im list

(** [(>>=)] : [bind] with reversed arguments. *)
val (>>=) : 'elt list -> ('elt -> 'im list) -> 'im list 


(** [range from upto] is the list of integers from [from] to [upto]
    both included. *)
val range : int -> int -> int list
