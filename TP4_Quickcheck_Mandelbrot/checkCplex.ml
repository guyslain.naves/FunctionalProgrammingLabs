
open Cplex
let pi = acos (-1.)


let angle : float QCheck.arbitrary =
  (* QCheck.(map_same_type (fun fl -> mod_float fl (2. *. pi)) float) *)
  let open QCheck in 
  make
    ~print:Print.float
    Gen.(float >|= fun t -> mod_float t (2. *. pi))
    


let rec repeat ~f ~n init =
  if n <= 0 then init
  else repeat ~f ~n:(n-1) (f init)

    
let analyse_test_result test test_result =
  let open QCheck.Test in
  let open QCheck.TestResult in
  let test_name = match get_name test with
    | Some name -> name
    | None -> "anonymous test"
  in
  let arbitrary = get_arbitrary test in 
  match test_result.state with
  | Success -> Printf.printf "%s: Success.\n" test_name
  | Failed failures ->
    Printf.printf "%s: Counter examples :\n" test_name;
    List.iter (fun fail -> Printf.printf "%s\n" (print_c_ex arbitrary fail)) failures
  | Error (instance, exceptn) ->
    print_error arbitrary test_name (instance, exceptn)
  |> Printf.printf "Exception:\n%s"
    
  

let law_zero_neutrality complex = complex +: zero = complex
let test_zero_neutrality =
  QCheck.Test.make
    ~count:100
    ~max_fail:10
    ~name:"Neutrality of zero for the addition"
    Cplex.arbitrary
    law_zero_neutrality


let law_three_120_is_identity complex =
  Cplex.are_almost_equal complex
    (repeat ~f:(rotate (2. *. pi /. 3.)) ~n:3 complex)
let test_third_rotation =
  QCheck.Test.make
    ~count:100
    ~max_fail:10
    ~name:"2pi/3 makes a rotation whose cube is identity"
    Cplex.arbitrary
    law_three_120_is_identity

let law_rotation_composition (theta1, theta2, complex) =
  are_almost_equal
    ( complex
      |> rotate (theta1 +. theta2)
    )
    ( complex
      |> rotate theta1
      |> rotate theta2
    )
let test_rotation_composition =
  QCheck.Test.make
    ~count:100
    ~max_fail:10
    ~name:"Composition of two rotations is a rotation of summed angles"
    QCheck.(triple angle angle Cplex.arbitrary)
    law_rotation_composition

let law_i_is_90_rotation complex =
  let image = i *: complex in 
  real image = -. imaginary complex
  && imaginary image = real complex
  
let test_i_is_90_rotation =
  QCheck.Test.make
    ~count:100
    ~max_fail:10
    ~name:"i is the rotation by 90 degrees"
    Cplex.arbitrary
    law_i_is_90_rotation


let law_inverse_is_inverse complex =
  are_almost_equal (complex *: invert complex) one

let test_inverse_is_inverse =
  QCheck.Test.make
    ~count:100
    ~max_fail:10
    ~name:"invert computes the inverse"
    Cplex.arbitrary
    law_inverse_is_inverse
  


let run_test (QCheck.Test.Test cell) =
  QCheck.Test.check_cell cell
  |> analyse_test_result cell
  
let run_all_test =
  print_newline ();
  List.iter run_test
    [
      test_zero_neutrality;
      test_third_rotation;
      test_rotation_composition;
      test_i_is_90_rotation;
      test_inverse_is_inverse
    ]

