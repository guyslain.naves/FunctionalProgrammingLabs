let max_iteration = 800
let max_radius = 1e3
let max_sq_radius = max_radius *. max_radius



type param_polynom =
  { degree : int;
    poly : Cplex.t -> Cplex.t -> Cplex.t;
    is_in_black_area : Cplex.t -> bool
  }

  
let out_of_bound (i,z_i) =
  i > max_iteration
  || Cplex.norm2 z_i > max_sq_radius
    

let smooth ~polynom (n,z_n) =
  let x = log (Cplex.norm2 z_n) /. log max_radius in
  float n -. (log x /. log (float polynom.degree))


let divergence polynom complex =
  if polynom.is_in_black_area complex then float (max_iteration + 1)
  else
    let (i,z_i) =
      Sequence.first_term
        ~fct:(polynom.poly complex)
        ~satisfying:out_of_bound
        (0,Cplex.zero)
    in
    smooth ~polynom (i,z_i)


let associate_divergence_to_pixel ~box ~polynom pixel =
  let complex = View.complex_of_pixel ~box pixel in
  (pixel, divergence polynom complex)


let compare_divergence (pix1,div1) (pix2,div2) = compare div1 div2


let sort_pixels ~box ~polynom =
  Plot.points ()
  |> List.rev_map (associate_divergence_to_pixel ~box ~polynom)
  |> List.filter (fun (pixel,div) -> div < float max_iteration)
  |> List.sort compare_divergence
  |> List.rev_map fst 



let draw ~box ~polynom ~gradient =
  let pixels = sort_pixels ~box ~polynom in
  let colors = Histogram.make ~size:(List.length pixels) ~descr:gradient in
  Plot.draw pixels colors
