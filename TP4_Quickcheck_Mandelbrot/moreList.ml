(* redefines some list functions more efficiently *)

let rev_concat lists =
  let append accu list =
    List.rev_append (List.rev list) accu
  in
  List.fold_left append [] lists
    
let bind fct list =
  list
  |> List.rev_map fct
  |> rev_concat
  
let (>>=) list fct = bind fct list

let range from upto =
  let rec loop accu current = 
    if current < from then accu
    else loop (current::accu) (current - 1)
  in
  loop [] upto

  
