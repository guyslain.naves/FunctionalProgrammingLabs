
type param_polynom =
  { degree : int;
    poly : Cplex.t -> Cplex.t -> Cplex.t;
    is_in_black_area : Cplex.t -> bool
  }

val draw :
  box:Gg.box2 ->
  polynom:param_polynom ->
  gradient:Histogram.gradient_description ->
  unit 
