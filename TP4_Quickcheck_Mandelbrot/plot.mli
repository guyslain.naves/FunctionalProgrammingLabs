(** A type for window pixels *)
type pixel

(** [x pix] is the relative horizontal position of the pixel, between 0 and 1 *)
val x : pixel -> float

(** [y pix] is the relative vertical position of the pixel, between 0 and 1 *)
val y : pixel -> float 

(** [points ()] is the list of all pixels of the graphic window. *)
val points : unit -> pixel list


val draw : pixel list -> Gg.color list -> unit
