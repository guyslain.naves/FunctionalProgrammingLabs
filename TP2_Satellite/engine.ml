module CanvasWidget =
struct

  type t =
    { canvas : Dom_html.canvasElement Js.t;
      image : Vg.image;
      view : Gg.box2;
      size : Gg.v2;
    }


  let canvas_of t = t.canvas

  let set_image image t =
    { t with image }

  let scale_view ~x ~y view =
    let open Gg in 
    let center = Box2.mid view in
    let size = Box2.size view in
    let new_size = V2.mul size (V2.v x y) in
    let new_origin = V2.sub center (V2.smul 0.5 new_size) in
    Box2.v new_origin new_size


  let adapt_view_to_size size view =
    let open Gg in 
    let size_aspect = V2.x size /. V2.y size in
    let view_aspect = V2.x (Box2.size view) /. V2.y (Box2.size view) in
    if size_aspect > view_aspect then
      scale_view ~x:(size_aspect /. view_aspect) ~y:1. view
    else
      scale_view ~x:1. ~y:(view_aspect /. size_aspect) view 


  let set_view view t = { t with view = adapt_view_to_size t.size view }

  

  let zoom ?(factor=1.25) widget =
    { widget with
      view = scale_view ~x:factor ~y:factor widget.view
    }


  let zoom_in = zoom ~factor:0.8
  let zoom_out = zoom ~factor:1.25




  let translate ~from:point to_position widget =
    let open Gg in 
    let vec = V2.sub to_position point in
    { widget with
      view = Box2.move vec widget.view
    }


  let render widget =
    let open Vg in 
    let renderer = Vgr.create (Vgr_htmlc.target widget.canvas) `Other in
    let box = adapt_view_to_size widget.size widget.view in
    ignore (Vgr.render renderer (`Image (widget.size, box, widget.image)));
    ignore (Vgr.render renderer `End);
    ()
    
  let create ~view ~size ?parent_id image =
    let canvas = Dom_html.(createCanvas document) in
    canvas##.width := int_of_float (Gg.V2.x size);
    canvas##.height := int_of_float (Gg.V2.y size);
    let widget =
      { canvas; view; size; image }
    in
    render widget;
    let parent =
      match parent_id with
      | None -> Dom_html.document##.body
      | Some id -> Dom_html.getElementById id
    in 
    Dom.appendChild parent canvas;
    widget



end
open CanvasWidget 

module Event = 
struct 

  type t =
    | KeyDown of string 
    | KeyUp of string
    | Click of Gg.v2
    | Tick 

  let key_up code = KeyUp code
  let key_down code = KeyDown code
  let click = Click Gg.V2.zero
  let tick = Tick 
  
  let match_event event (ev,_) =
    match event,ev with
    | KeyDown c1, KeyDown c2 -> c1 = c2
    | KeyUp c1, KeyUp c2 -> c1 = c2
    | Click _, Click _ -> true
    | Tick, Tick -> true
    | _, _ -> false
      
  let get_vector_of_event mouse_event widget =
    let rect = widget.canvas##getBoundingClientRect in
    let (x_pix,y_pix) =
      (float (mouse_event##.clientX) -. rect##.left,
       float (mouse_event##.clientY) -. rect##.top)
    in
    let (x_max,y_max) =
      (float (widget.canvas##.width),
       float (widget.canvas##.height)
      )
    in
    let (x_max,y_max) =
      (Js.Optdef.case (rect##.width) (fun () -> x_max) (fun fl -> fl),
       Js.Optdef.case (rect##.height) (fun () -> y_max) (fun fl -> fl)
      )
    in
    let open Gg in 
    let normalized = V2.v (x_pix /. x_max) ((y_max -. y_pix) /. y_max) in
    Box2.size widget.view
    |> V2.mul normalized
    |> V2.add (Box2.o widget.view)


  let set_timer dt =
    let (waiting, waker) = Lwt.wait () in
    let callback =
      Js.wrap_callback (Lwt.wakeup waker)
    in 
    ignore (Dom_html.window##setTimeout callback (dt *. 1e-3));
    waiting

  let keys_not_to_propagate =
    [ "ArrowDown"; "ArrowUp"; "ArrowLeft"; "ArrowRight" ]
  let stop_propagation_if_necessary ev code =
    if List.mem code keys_not_to_propagate then
      Dom.preventDefault ev
    
  
  let wait_next_event waiting_timer widget =
    let open Lwt in
    let js_opt_to_string opt =
      match Js.Optdef.to_option opt with
      | Some string -> Js.to_string string
      | None -> "none"
    in 
    Lwt.pick 
      [ ( Lwt_js_events.click widget.canvas >>= fun ev ->
          Printf.printf "click\n";
          return (waiting_timer, Click (get_vector_of_event ev widget))
        );
        ( Lwt_js_events.keydown Dom_html.document >>= fun ev ->
          let code = js_opt_to_string ev##.key in
          stop_propagation_if_necessary ev code;
          Printf.printf "down %s\n" code;
          return (waiting_timer, KeyDown code)
        );
        ( Lwt_js_events.keyup Dom_html.document >>= fun ev ->
          let code = js_opt_to_string ev##.key in
          stop_propagation_if_necessary ev code;
          Printf.printf "up %s\n" code;
          return (waiting_timer, KeyUp code)
        );
        ( waiting_timer >>= fun () ->
          return (waiting_timer, Tick)
        )
      ]

end



type 'model app =
  { model : 'model
  ; event_handlers : (Event.t * ('model -> 'model)) list
  ; show : 'model -> Vg.image
  ; dt : float
  ; stop : 'model -> bool
  }


let rec loop ?waiting_timer canvas app =
  let open Lwt in
  let waiting_timer =
    match waiting_timer with
    | Some left -> left
    | None -> Event.set_timer app.dt
  in 
  Event.wait_next_event waiting_timer canvas >>= fun (timer, event) -> 
  let updated_model = 
    if List.exists Event.(match_event event) app.event_handlers then
      let (_,up) = List.find Event.(match_event event) app.event_handlers in
      up app.model
    else
      app.model
  in
  let updated_app = { app with model = updated_model } in  
  if event = Event.Tick then
    ( let canvas = 
        set_image (app.show app.model) canvas
      in
      render canvas;
      if app.stop app.model then return ()
      else loop canvas updated_app
    )
  else
    loop ~waiting_timer canvas updated_app


let run ?(width=800) ?(height=600) ?size ?parent_id app =
  let view =
    Gg.Box2.v
      (Gg.V2.v (-. float width /. 2.) (-. float height /. 2.))
      (Gg.V2.v (float width) (float height))
  in
  let size =
    match size with
    | None -> Gg.V2.v (float width) (float height)
    | Some s -> s 
  in
  let canvas =
    create
      ~view
      ~size
      ?parent_id
      (app.show app.model)
  in
  Lwt.async (fun () -> loop canvas app)
