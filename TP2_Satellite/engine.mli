module CanvasWidget :
sig
  type t

  val create : view:Gg.box2 -> size:Gg.v2 -> ?parent_id:string -> Vg.image -> t 
  
  val set_image : Vg.image -> t -> t

  val set_view : Gg.box2 -> t -> t

  
end


module Event :
sig
  type t

  val key_up : string -> t
    
  val key_down : string -> t

  val click : t

  val tick : t

end


type 'model app =
  { model : 'model
  ; event_handlers : (Event.t * ('model -> 'model)) list
  ; show : 'model -> Vg.image
  ; dt : float (** in second *)
  ; stop : 'model -> bool
  }


val run :
  ?width:int -> ?height:int
  -> ?size:Gg.v2 -> ?parent_id:string
  -> 'model app -> unit

