let epsilon = 1e-9
let is_zero fl = abs_float fl < epsilon 
 
(** ax + b = 0 *)
let degree1 a b =
  if is_zero a then None
  else
    let r = b /. a in
    if r > 0. then Some r
    else None

(** ax^2 + bx + c = 0 *)
let degree2 a b c =
  if is_zero a then degree1 b c
  else
    let delta = b *. b -. 4. *. a *. c in
    if delta < 0. then None
    else
      let r = (sqrt delta -. b) /. (2. *. a) in
      if r > epsilon then Some r
      else None
