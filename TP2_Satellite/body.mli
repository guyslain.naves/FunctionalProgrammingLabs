val gravity : float


type t =
  { position : Gg.p2;
    velocity : Gg.v2;
    mass : float;
    radius : float;
  }

val create :
  position:Gg.p2 ->
  velocity:Gg.v2 ->
  mass:float ->
  radius:float ->
  t


val move : dt:float -> force:Gg.v2 -> t -> t



(** [orbital_speed ~around:body pos] is the velocity needed for an
    object at point [pos] to be in spherical orbit around [body] *)
val orbital_speed : around:t -> Gg.p2 -> Gg.v2


(** the force exerced on [attracted] by [attracting] *)
val gravitational_force :
  attracting:t ->
  attracted:t ->
  Gg.v2
    
