let epsilon = 1e-9
let is_zero fl = abs_float fl < epsilon 
let sq fl = fl *. fl


let gravity = 4.


type t = {
  position : Gg.p2;
  velocity : Gg.v2;
  mass : float;
  radius : float;
}

let create ~position ~velocity ~mass ~radius =
  { position; velocity; mass; radius }

let move ~dt ~force ball =
  let position = Gg.V2.(smul dt ball.velocity + ball.position) in
  let delta_vel = Gg.V2.(smul (dt /. ball.mass) force) in
  let velocity = Gg.V2.(ball.velocity + delta_vel) in
  { ball with position; velocity }


(* NOT TESTED *)
(* val next_collision_time : t -> t -> float option *)
(* val are_in_collision : t -> t -> bool *)
(* val resolve_collision : t -> t -> t * t *)

(* let next_collision_time ball1 ball2 = *)
(*   let pos = Gg.V2.(ball1.position - ball2.position) in *)
(*   let vel = Gg.V2.(ball2.velocity - ball1.velocity) in *)
(*   Roots.degree2 *)
(*     (Gg.V2.norm2 pos) *)
(*     (2. *. Gg.V2.dot pos vel) *)
(*     (Gg.V2.norm2 vel) *)


(* let resolve_collision ball1 ball2 = *)
(*   let normal = Gg.V2.(unit (ball1.position - ball2.position)) in *)
(*   let mass = ball1.mass +. ball2.mass in *)
(*   let delta_vel = Gg.V2.(ball1.velocity - ball2.velocity) in *)
(*   let deviation = Gg.V2.(smul (dot delta_vel normal) normal) in  *)
(*   ( { ball1 with *)
(*       velocity = *)
(*         Gg.V2.(ball1.velocity - smul (2. *. ball2.mass /. mass) deviation) *)
(*     }, *)
(*     { ball2 with *)
(*       velocity = *)
(*         Gg.V2.(ball2.velocity + smul (2. *. ball1.mass /. mass) deviation) *)
(*     } *)
(*   ) *)

(* let are_in_collision ball1 ball2 = *)
(*   let dist = Gg.V2.(norm2 (ball1.position - ball2.position)) in *)
(*   let r = sq ball1.radius +. sq ball2.radius in *)
(*   is_zero (dist -. r) && *)
(*   let delta_pos = Gg.V2.(ball1.position - ball2.position) in  *)
(*   let delta_vel = Gg.V2.(ball1.velocity - ball2.velocity) in *)
(*   Gg.V2.dot delta_pos delta_vel < 0. *)



let orbital_speed ~around:star position =
  let open Gg.V2 in 
  let relative_position = position - star.position in
  let dist_to_center = norm relative_position in 
  relative_position
  |> unit
  |> ltr (Gg.M2.rot2 Gg.Float.pi_div_2)
  |> smul (sqrt (gravity *. star.mass /. dist_to_center))


let gravitational_force ~attracting ~attracted =
  let relative_position = Gg.V2.(attracting.position - attracted.position) in
  let dist2 = Gg.V2.(norm2 relative_position) in
  if is_zero dist2 then
    Gg.V2.zero
  else
    let dir = Gg.V2.(unit relative_position) in
    Gg.V2.smul (gravity *. attracting.mass *. attracted.mass /. dist2) dir

