let dt = 0.02

let () = Random.self_init ()

type model =
  { satellite : Satellite.t;
    stars : (Gg.v2 * Gg.color) list
  }


let colors =
  List.flatten
    [ Colors.gradient ~n:10 Colors.white Colors.lightcyan
    ; Colors.gradient ~n:10 Colors.white Colors.lightcoral
    ; Colors.gradient ~n:10 Colors.white Colors.lightgreen
    ]

let random_color ()  =
  let n = List.length colors in
  let p = Random.int n in
  List.nth colors p 
  
let random_star () =
  let x = Random.float 800. in
  let y = Random.float 800. in
  ( Gg.V2.v (x -. 400.) (y -. 400.)
  , random_color ()
  )

let rec random_star_list len =
  if len = 0 then []
  else random_star () :: random_star_list (len-1)


let model =
  { satellite =
      Satellite.create
        ~radius:3.
        ~mass:1.
        ~color:Gg.Color.red 
        ~fuel:10.
        ~thrust:10. 
        (Gg.V2.v 150. 150.);
    stars = random_star_list 20
  }

let lift action model =
  { model with
    satellite = action model.satellite
  }
  

let accelerate model =
  lift Satellite.start_accelerating

let stop_acceleration model =
  lift Satellite.stop_accelerating


let update model =
  { model with 
    satellite =
      model.satellite
      |> Satellite.move ~dt
      |> Satellite.recharge_fuel ~dt Space.star
  }


let event_handlers  =
  let open Engine.Event in 
  [ (key_down "ArrowLeft", lift Satellite.start_rotation_left);
    (key_up "ArrowLeft", lift Satellite.stop_rotation);
    (key_down "ArrowRight", lift Satellite.start_rotation_right);
    (key_up "ArrowRight", lift Satellite.stop_rotation);
    (key_down "ArrowUp", lift Satellite.start_accelerating);
    (key_up "ArrowUp", lift Satellite.stop_accelerating);
    (tick, update)
  ]
    



let show_fuel fuel=
  let open Vg in
  let width = 12. in
  let height = 792. in
  let corner = Gg.V2.v (-397.) (-397.) in 
  let background =
    I.const Colors.indigo
    |> I.cut (P.rect Gg.(Box2.v corner (V2.v width height)) P.empty)
  in
  let foreground =
    I.const Colors.mediumorchid
    |> I.cut (P.rect Gg.(Box2.v corner (V2.v width (fuel *. height))) P.empty)
  in
  background
  |> I.blend foreground

let show_star (center, color) =
  let open Vg in 
  I.const color
  |> I.cut (P.circle center 1. P.empty)

let show_background stars =
  let open Vg in 
  stars
  |> List.map show_star
  |> fun list -> List.fold_right I.blend list (I.const Gg.Color.black)
  

let show model =
  let open Vg in 
  show_background model.stars
  |> I.blend Space.show_star
  |> I.blend (Satellite.show model.satellite)
  |> I.blend (show_fuel (Satellite.fuel_ratio model.satellite))



let stop model =
  let sat_position = 
    model.satellite.Satellite.body.Body.position
  in
  let star_position =
    Space.star.Body.position
  in
  let dist = Gg.V2.(norm ( sat_position - star_position)) in
  dist < Space.star.Body.radius


let main =
  let open Engine in
  run
    ~width:800
    ~height:800
    ~size:(Gg.V2.v 200. 200.)
    ~parent_id:"script__satellite"
    { model;
      event_handlers;
      show;
      dt;
      stop;
    }
