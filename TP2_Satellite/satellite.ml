type rotation =
  | Left
  | Right
  | Stay

type t =
  { body : Body.t
  ; color : Gg.color 
  ; direction : Gg.V2.t
  ; angular_speed : float
  ; fuel : float
  ; fuel_capacity : float 
  ; thrust : float 
  ; is_accelerating : bool
  ; rotation : rotation 
  }


let create ~radius ~mass ~color ~fuel ~thrust position =
  let velocity = Body.orbital_speed ~around:Space.star position in
  let direction = Gg.V2.unit velocity in 
  { body =
      Body.create
        ~position
        ~velocity
        ~mass
        ~radius
  ; color
  ; direction
  ; angular_speed = Gg.Float.pi /. 2.
  ; fuel
  ; fuel_capacity = fuel
  ; thrust
  ; is_accelerating = false
  ; rotation = Stay
  }

let fuel_ratio t = t.fuel /. t.fuel_capacity

let start_rotation_left t =
  { t with rotation = Left }

let start_rotation_right t =
  { t with rotation = Right }

let stop_rotation t =
  { t with rotation = Stay }

let start_accelerating t =
  { t with is_accelerating = true }

let stop_accelerating t =
  { t with is_accelerating = false }

let recharge_fuel ~dt star t =
  let star_power = 1500. in
  let star_direction = Gg.V2.(star.Body.position - t.body.Body.position) in
  let alignment =  
    abs_float (Gg.V2.(dot t.direction (unit star_direction)))
  in 
  let gain =
    dt *. star_power /. (Gg.V2.norm star_direction ** 2.) *. alignment
  in 
  { t with
    fuel = min t.fuel_capacity (t.fuel +. gain)
  }


let rotate_left ~dt t =
  let angle = t.angular_speed *. dt in
  let mat = Gg.M2.rot2 angle in
  { t with
    direction = Gg.V2.ltr mat t.direction
  }

let rotate_right ~dt t =
  let angle = -. t.angular_speed *. dt in
  let mat = Gg.M2.rot2 angle in
  { t with
    direction = Gg.V2.ltr mat t.direction
  }

let move ~dt t =
  let open Gg.V2 in 
  let motor_force = 
    if t.is_accelerating && t.fuel > dt then
      t.thrust * t.direction / t.body.Body.mass
    else zero
  in
  let force =
    motor_force 
    + Body.gravitational_force
      ~attracting:Space.star
      ~attracted:t.body
  in
  let rotate =
    match t.rotation with
    | Stay -> (fun t -> t)
    | Left -> rotate_left ~dt
    | Right -> rotate_right ~dt
  in 
  rotate
    { t with
      body = Body.move ~dt ~force t.body; 
      fuel = if t.is_accelerating then max 0. (t.fuel -. dt) else t.fuel
    }



let left_panel t =
  let open Vg in
  let path = 
    P.empty
    |> P.rect Gg.(Box2.v (V2.v (-1.) (-5.)) (V2.v 2. 4.))
  in 
  I.const t.color
  |> I.cut ~area:`Anz path 

let right_panel t =
  let open Vg in
  let path = 
    P.empty
    |> P.rect Gg.(Box2.v (V2.v (-1.) (1.)) (V2.v 2. 4.))
  in 
  I.const t.color
  |> I.cut ~area:`Anz path 

let body t =
  let open Vg in
  let path =
    P.empty
    |> P.circle (Gg.V2.v 0. 0.) 1. 
  in
  I.const Gg.Color.red 
  |> I.cut ~area:`Anz path
    

let motor t =
  let open Vg in
  let path = 
    P.empty
    |> P.rect Gg.(Box2.v (V2.v (-2.0) (-0.5)) (V2.v 1.5 1.))
  in 
  I.const t.color
  |> I.cut ~area:`Anz path

let flame t =
  if t.is_accelerating && t.fuel > 0. then
    let open Vg in
    let path =
      P.empty
      |> P.sub (Gg.V2.v (-2.1) (-0.5))
      |> P.line (Gg.V2.v (-2.1) (0.5))
      |> P.line (Gg.V2.v (-5.) 0.)
      |> P.close
    in
    I.const Colors.gold
    |> I.cut ~area:`Anz path
  else
    Vg.I.void
      
let show t =
  let open Vg.I in
  left_panel t
  |> blend (right_panel t)
  |> blend (motor t)
  |> blend (body t)
  |> blend (flame t)
  |> scale Gg.V2.(v t.body.Body.radius t.body.Body.radius)
  |> rot (Gg.V2.angle t.direction)
  |> move t.body.Body.position




