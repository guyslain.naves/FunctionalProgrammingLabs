
let star =
  Body.create
    ~position:Gg.V2.zero
    ~velocity:Gg.V2.zero
    ~mass:5e4
    ~radius:20.

let star_color =
  Colors.yellow

let show_star =
  let open Vg in 
  let path =
    P.empty
    |> P.circle Gg.V2.zero star.Body.radius
  in
  I.const star_color
  |> I.cut ~area:`Anz path

  
