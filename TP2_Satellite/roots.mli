(** [degree1 a b] is the positive root of [ax+b], if any *)
val degree1 : float -> float -> float option

(** [degree2 a bc] is the smallest positive root of [ax^2 + bx + c] *)
val degree2 : float -> float -> float -> float option 
