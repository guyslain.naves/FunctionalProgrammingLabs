(** Shapes applicable to basic bricks. *)


(** A shape is a [Vg.path] that should be inside the unit square (with
    corners [{0,1} x {0,1}]). The shape will be deformed to fit the size
    of the brick.*)
type t = Vg.path


(** The default shape is q full square *)
val default : t 

(** Solid ellipsoidal shape. *)
val circle : t

(** Solid rectangular shape, same as default. *)
val square : t

(** Four orientations for solid triangles. *)
val bottom_left_triangle : t
val top_left_triangle : t
val bottom_right_triangle : t
val top_right_triangle : t


(** Four kind of rectangular borders, with different widths. *)
val outlined : thinness:float -> t
val very_thin : t
val thin : t
val thick : t 

