let brick =
  let open Generator in
  block ~width:2 ~height:1

let two_bricks =
  let open Generator in
  blue brick
  |> left_of (red brick)

let three_bricks =
  let open Generator in
  blue brick
  |> left_of (red brick)
  |> left_of (green brick)

let holed_two_bricks =
  let open Generator in
  brick
  |> left_of ~gap:2 brick

let main =
  let open Renderer in
  generate_and_render holed_two_bricks
