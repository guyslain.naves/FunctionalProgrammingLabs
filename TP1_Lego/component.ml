module IntMap = Map.Make(struct type t = int let compare = compare end)
module BlockSet = MergeableSet

type t =
  { blocks : Block.t BlockSet.t;
    profile_down : int IntMap.t;
    profile_up : int IntMap.t;
    profile_left : int IntMap.t;
    profile_right : int IntMap.t;
  }

let _ =
  Printf.sprintf "(%d, %d)" 3 4 

let show comp =
  let pr_pair (i, j) = Printf.sprintf "(%d,%d)" i j in 
  [ "up:";
    IntMap.bindings comp.profile_up
    |> List.map pr_pair
    |> String.concat ", ";
    "down:";
    IntMap.bindings comp.profile_down
    |> List.map pr_pair
    |> String.concat ", ";
    "right:";
    IntMap.bindings comp.profile_right
    |> List.map pr_pair
    |> String.concat ", ";
    "left:";
    IntMap.bindings comp.profile_left
    |> List.map pr_pair
    |> String.concat ", "
  ]
  |> String.concat "\n"
    
  

let find_option index map =
  if IntMap.mem index map then
    Some (IntMap.find index map)
  else
    None
  
let min_in_column column component =
  find_option column component.profile_down
let max_in_column column component =
  find_option column component.profile_up
let min_in_row row component =
  find_option row component.profile_left
let max_in_row row component =
  find_option row component.profile_right
  
(* need a monoid to simplify the algorithms, but should stay private *)
let empty =
  { blocks = BlockSet.empty;
    profile_down = IntMap.empty;
    profile_up = IntMap.empty;
    profile_left = IntMap.empty;
    profile_right = IntMap.empty
  }

let blocks component = component.blocks

let is_empty component = BlockSet.is_empty component.blocks
    

let update_profile fct value profile index =
  let new_value = 
    if IntMap.mem index profile then
      IntMap.find index profile
      |> fct value
    else
      value
  in
  IntMap.add index new_value profile


let range mini maxi =
  let rec push ~up_to:maxi stack =
    if maxi < mini then stack
    else push ~up_to:(maxi-1) (maxi::stack)
  in
  push ~up_to:maxi []


let add_block block component =
  let (x,y) = block.Block.position in
  let (width,height) = (block.Block.width, block.Block.height) in 
  { blocks =
      BlockSet.add block component.blocks;
    profile_down =
      List.fold_left
        (update_profile min y)
        component.profile_down 
        (range x (x + width - 1));
    profile_up =
      List.fold_left
        (update_profile max (y + height - 1))
        component.profile_up
        (range x (x + width - 1));
    profile_left =
      List.fold_left
        (update_profile min x)
        component.profile_left
        (range y (y + height - 1));
    profile_right =
      List.fold_left
        (update_profile max (x + width - 1))
        component.profile_right
        (range y (y + height - 1))
  }


let singleton block =
  add_block block empty

let of_set blocks =
  BlockSet.fold
    ~f:(fun component block -> add_block block component)
    empty
    blocks 
    

let block ~width ~height =
  singleton (Block.block width height)


let map ~f:on_blocks component =
  component.blocks
  |> BlockSet.map ~f:on_blocks
  |> of_set

let traverse ~f component state =
  let (blocks,state) = BlockSet.traverse ~f component.blocks state in
  (of_set blocks, state)

let for_each ~f component =
  BlockSet.for_each ~f component.blocks


let merge component1 component2 =
  BlockSet.fold
    ~f:(fun component block -> add_block block component)
    component1
    component2.blocks

let x_shift delta_x =
  map ~f:(Block.move (delta_x,0))
let y_shift delta_y =
  map ~f:(Block.move (0,delta_y))
let move vec =
  map ~f:(Block.move vec)


let vertical_gap ~below:component1 ~above:component2 =
  let max_with_delta_in_column column col_height delta =
    match min_in_column column component2 with
    | Some base -> min delta (base - col_height - 1)
    | None -> delta
  in
  let delta = 
    IntMap.fold
      max_with_delta_in_column
      component1.profile_up
      max_int
  in
  if delta = max_int then 0 else delta

let horizontal_gap ~left:component1 ~right:component2 =
  let max_with_delta_in_row row row_width delta =
    match min_in_row row component2 with
    | Some base -> min delta (base - row_width - 1)
    | None -> delta
  in
  let delta =
    IntMap.fold
      max_with_delta_in_row
      component1.profile_right
      max_int
  in
  if delta = max_int then 0 else delta




  
  
