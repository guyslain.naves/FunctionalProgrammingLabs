
let one_blue_triangle_brick_gen =
  let open Generator in
  bottom_left_triangle (blue (block ~width:2 ~height:1))

let one_blue_triangle_brick_gen_with_pipe =
  let open Generator in
  block ~width:2 ~height:1 |> blue |> bottom_left_triangle

let () =
  let open Renderer in 
  generate_and_render one_blue_triangle_brick_gen
