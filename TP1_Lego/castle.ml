let grays =
  let open Colors in
  Combinator.gradient
    ~n:100
    Pure.silver
    Pure.gray

let blues =
  let open Colors in
  Combinator.gradient
    ~n:100
    Pure.cornflowerblue
    Pure.steelblue


let browns =
  let open Colors in
  Combinator.gradient
    ~n:100
    Pure.saddlebrown
    Pure.maroon


let brick =
  let open Generator in
  sample grays @@ fun color ->
  block ~width:2 ~height:1 |> color

let hbrick =
  let open Generator in
  sample grays @@ fun color ->
  block ~width:1 ~height:1 |> color

let tile =
  let open Generator in
  sample blues @@ fun color ->
  block ~width:1 ~height:1 |> color

let wall_layer ~width =
  let open Generator in
  if width = 1 then
    hbrick
  else if width mod 2 = 0 then
    pack_horizontally ~n:(width/2) brick
  else
    pack_horizontally ~n:(width/2) brick |> left_of hbrick

let shifted_wall_layer ~width =
  let open Generator in 
  if width = 1 then
    hbrick
  else
    hbrick |> left_of (wall_layer ~width:(width-1))


let rec wall ~width ~height =
  let open Generator in 
  if height = 1 then
    wall_layer ~width
  else if height mod 2 = 0 then
    shifted_wall_layer ~width
    |> above (wall_layer ~width)
    |> pack_vertically ~n:(height/2)
  else
    wall_layer ~width
    |> above (wall ~width ~height:(height-1))


let crenellation ~width =
  let open Generator in
  ( if width mod 3 = 2 then
      pack_horizontally ~gap:1 ~n:(width / 3 + 1) brick
    else if width mod 3 = 1 then
      pack_horizontally ~gap:1 ~n:(width / 3) brick |> x_shift 1
    else
      pack_horizontally ~gap:1 ~n:(width / 3) brick
  )
  |> pack_vertically ~n:3


let rampart_wall ~width ~height =
  let open Generator in
  crenellation ~width
  |> above (wall ~width ~height)



let arrowslit_wall ~width =
  let open Generator in
  if width < 7 then
    wall ~width ~height:4
  else
    let nb_slits = (width - 3) / 4 in
    let hnb_slits = (nb_slits + 1) / 2 in 
    let w1 = (width - nb_slits) / (nb_slits + 1) in
    let w2 = width - nb_slits * w1 - nb_slits in
    let subwall1 = wall ~width:w1 ~height:4 in
    let subwall2 = wall ~width:w2 ~height:4 in
    pack_horizontally ~gap:1 ~n:hnb_slits subwall1
    |> left_of ~gap:1 subwall2 
    |> left_of ~gap:1
      (pack_horizontally ~gap:1 ~n:(nb_slits - hnb_slits) subwall1)

let tile_layer n =
  let open Generator in
  if n = 0 then
    bottom_right_triangle tile
    |> left_of (bottom_left_triangle tile)
  else
    bottom_right_triangle tile
    |> left_of (pack_horizontally ~n tile)
    |> left_of (bottom_left_triangle tile)

let rec shrinking ~width height =
  let open Generator in
  if height = 1 then
    tile_layer width |> x_shift (-1)
  else
    tile_layer width
    |> above (shrinking ~width:(width+2) (height-1))
    |> x_shift (-1)

let rec growing ~width height =
  let open Generator in
  let layer n =
    top_right_triangle hbrick
    |> left_of (wall_layer ~width:n)
    |> left_of (top_left_triangle hbrick)
  in
  if height = 1 then
    layer width |> x_shift (-1)
  else
    growing ~width:(width+2) (height-1)
    |> above (layer width)
    |> x_shift (-1)


let tower_base = wall

let tower_middle_1 ~width =
  let open Generator in 
  if width < 6 then
    wall ~width ~height:12
  else
    wall ~width ~height:4
    |> above (arrowslit_wall ~width)
    |> above (wall ~width ~height:4)

let tower_middle_2 ~width =
  let open Generator in
  shrinking ~width 4 
  |> above (arrowslit_wall ~width:(width+8) |> x_shift (-4))
  |> above (growing ~width 4)
  |> above (wall ~width ~height:4)

let tower_middle_3 ~width =
  let open Generator in
  let middle =
    wall ~width:4 ~height:2
    |> left_of (arrowslit_wall ~width)
    |> left_of (wall ~width:4 ~height:2)
    |> x_shift (-4)
  in 
  wall ~width ~height:4
  |> above middle
  |> above (growing ~width 4)


let tower_middle ~width =
  let open Generator in 
  sample
    [ tower_middle_1;
      tower_middle_2;
      tower_middle_3
    ]
  @@ fun choice -> choice ~width



let rec tower_top_1 ~width =
  let open Generator in
  if width = 2 then
    tile_layer 0
  else
    tower_top_1 ~width:(width - 2)
    |> x_shift 1
    |> above (tile_layer (width - 2))

let rec tower_top_2 ~width =
  let open Generator in
  rampart_wall ~width ~height:4

let tower_top ~width =
  let open Generator in
  sample [tower_top_1; tower_top_2] @@ fun top ->
  top ~width


let tower ~width ~base_height =
  let open Generator in
  sample ~chances:[4;2;1;1] [1;2;3;4] @@ fun nb_middles ->
  tower_top ~width
  |> above (pack_vertically ~n:nb_middles (tower_middle ~width))
  |> above (tower_base ~width:width ~height:base_height)


let castle =
  let open Generator in 
  tower ~width:14 ~base_height:20
  |> left_of (rampart_wall ~width:20 ~height:14)
  |> Generator.pack_horizontally ~n:6
  |> left_of (tower ~width:14 ~base_height:20)
  |> x_shift 4



let params =
  let open Renderer in
  let open Gg in 
  { default_params with
    view =
      Box2.v
        V2.(v 0. 0.)
        V2.(v 230. 230.)
  }

let main =
  let _ = Random.self_init () in
  let open Renderer in
  generate_and_render ~params castle
