
type t = Random.State.t -> (Component.t * Random.State.t)
let generate random gen = gen (Random.State.copy random)

let (>>=) : t -> (Component.t -> t) -> t = fun gen f state ->
  let (component, state) = gen state in
  f component state
let return component state = (component, state)


let block ~width ~height state =
  ( Component.block ~height ~width,
    state
  )



let left_of ?(gap=0) gen gen_left =
  gen_left >>= fun component_left ->
  gen >>= fun component ->
  let delta_x =
    max 0 gap - Component.horizontal_gap ~left:component_left ~right:component
  in
  component
  |> Component.x_shift delta_x
  |> Component.merge component_left
  |> return 

let right_of ?(gap=0) gen gen_right =
  gen_right >>= fun component_right -> 
  gen >>= fun component ->
  let delta_x =
    Component.horizontal_gap ~left:component ~right:component_right - max 0 gap
  in
  component
  |> Component.x_shift delta_x
  |> Component.merge component_right
  |> return 



let above gen gen_above =
  gen_above >>= fun component_above ->
  gen >>= fun component ->
  let delta_y =
    -Component.vertical_gap ~below:component ~above:component_above
  in
  component_above
  |> Component.y_shift delta_y
  |> Component.merge component
  |> return 


let below gen gen_below =
  gen_below >>= fun component_below ->
  gen >>= fun component ->
  let delta_y =
    Component.vertical_gap ~below:component_below ~above:component
  in
  component_below
  |> Component.y_shift delta_y
  |> Component.merge component
  |> return


let rec repeat ~n ~f init =
  if n = 0 then init
  else repeat ~n:(n-1) ~f (f init)

              
let pack_horizontally ?(gap=0) ~n gen =
  if (n < 1) then raise (Invalid_argument "pack_horizontally")
  else
    repeat
      ~n:(n-1)
      ~f:(left_of ~gap gen)
      gen 
    


let pack_vertically ~n gen =
  if (n < 1) then raise (Invalid_argument "pack_vertically") else 
  repeat
    ~n:(n-1)
    ~f:(fun big_gen -> gen |> above big_gen)
    gen



let map ~f gen =
  gen >>= fun component ->
  component
  |> Component.map ~f
  |> return 


let x_shift delta = map ~f:(Block.move (delta,0))
let y_shift delta = map ~f:(Block.move (0,delta))


let color color = map ~f:(Block.color color)
let shape shape = map ~f:(Block.shape shape)


let black = map ~f:Block.black
let red = map ~f:Block.red 
let blue = map ~f:Block.blue
let green = map ~f:Block.green
let white = map ~f:Block.white


let solid = map ~f:Block.solid
let very_thin = map ~f:Block.very_thin
let thin = map ~f:Block.thin
let thick = map ~f:Block.thick

let circle = map ~f:Block.circle
let square = map ~f:Block.square
let bottom_left_triangle = map ~f:Block.bottom_left_triangle
let bottom_right_triangle = map ~f:Block.bottom_right_triangle
let top_left_triangle = map ~f:Block.top_left_triangle
let top_right_triangle = map ~f:Block.top_right_triangle


let rec pick_choice chances values choice =
  match chances, values with
  | c::_, v::_ when choice < c -> v
  | c::chances, _::values -> pick_choice chances values (choice - c)
  | [], _ -> raise (Invalid_argument "Component.sample: not enough chances")
  | _, [] -> raise (Invalid_argument "Component.sample: too many chances")

let sample ?chances values to_gen random_state =
  let chances =
    match chances with
    | None -> List.map (fun v -> 1) values
    | Some chances -> chances
  in 
  let count_chances = 
    List.fold_left (+) 0 chances
  in
  if (count_chances <= 0) then
    raise (Invalid_argument "Component.sample: bad number of choices")
  else
    let choice =
      Random.State.int random_state count_chances
    in
    let value = 
      pick_choice chances values choice
    in
    to_gen value random_state

