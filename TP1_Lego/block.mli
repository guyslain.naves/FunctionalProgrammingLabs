type t = {
  position : (int * int);
  width : int;
  height : int;
  color : Gg.color;
  shape : Shape.t 
}

val unit : t 
val block : width:int -> height:int -> t 

val move : (int * int) -> t -> t
  
val color : Gg.color -> t -> t
  
val black : t -> t
val red : t -> t
val blue : t -> t
val green : t -> t
val white : t -> t

val shape : Shape.t -> t -> t

val solid : t -> t
val very_thin : t -> t
val thin : t -> t
val thick : t -> t

val circle : t -> t
val square : t -> t
val bottom_left_triangle : t -> t
val bottom_right_triangle : t -> t
val top_left_triangle : t -> t
val top_right_triangle : t -> t 
