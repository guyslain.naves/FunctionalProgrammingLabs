type 'elt t

val empty : 'elt t
val is_empty : 'elt t -> bool

val singleton : 'elt -> 'elt t

val merge : 'elt t -> 'elt t -> 'elt t

val add : 'elt -> 'elt t -> 'elt t

val map : f:('elt -> 'im) -> 'elt t -> 'im t

val catamorphism :
  at_leaf:'result ->
  on_node:('result -> 'elt -> 'result -> 'result) ->
  'elt t -> 'result

val fold :
  f:('state -> 'elt -> 'state) ->
  'state -> 'elt t -> 'state
    

val traverse :
  f:('elt -> 'state -> ('elt * 'state))
  -> 'elt t -> 'state -> ('elt t * 'state)

val for_each :
  f:('elt -> unit)
  -> 'elt t -> unit 
