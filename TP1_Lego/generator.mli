(** Generating brick constructions. 

    This module contains almost anything you need to build basic
    constructions from simple rectangular bricks, possibly making use
    of some random rules when needed. 
*)


(** The type of lego generators. These are functions from a random seed
   to a lego construction. *)
type t

(** [generate random_state generator] randomly builds a lego
   construction depending on the given seed. It returns the construction
   as well as a fresh random state. *)
val generate : Random.State.t -> t -> Component.t * Random.State.t 


(** [block ~width ~height] is a single brick with given dimensions. The
   bottom-left corner is placed at the origin. *)
val block : width:int -> height:int -> t 

(** [b1 |> left_of b2] is the concatenation of [b1] to the left of
   [b2]. [b1] is not moved. *)
val left_of : ?gap:int -> t -> t -> t

(** [b1 |> right_of b2] is the concatenation of [b1] to the right of
   [b2]. [b2] is not moved. *)
val right_of : ?gap:int -> t -> t -> t

(** [b1 |> above b2] is the stacking of [b1] above [b2]. [b2] is not moved. *)
val above : t -> t -> t

(** [b1 |> below b2] is the stacking of [b1] below [b2]. [b2] is not moved. *)
val below : t -> t -> t 



(** [pack_horizontally ~gap ~n gen] generates [n] components using
    [gen], with [n > 0], and places them in sequence horizontally with
    the given [gap] between two consecutive components (default gap is 0).
*)
val pack_horizontally : ?gap:int -> n:int -> t -> t

(** Similar to [pack_horizontally] but components are stacked. *)
val pack_vertically : n:int -> t -> t


(** [x_shift delta b] shifts the component [b] to the left by [delta] unit. *)
val x_shift : int -> t -> t
val y_shift : int -> t -> t 

(** [sample ~chances choices parametrized_gen] is a generator that
    chooses an item among [choices], and use this item as parameter
    for [parametrized_gen]. It may be used with the operator [@@]:
    [ sample choices @@ fun choice -> ... ]

    [chances], if given, must have the same length as [choices], and
    allows to give different probabilities to each item, in proportion
    with the given weight. If not given, probabilities are uniform.
*)
val sample : ?chances:int list -> 'value list -> ('value -> t) -> t 


(** All the combinators are intended to be used with the pipe
    application operator. So instead of writing:
    [red (circle (block ~width:2 ~height:1))]
    we would write:
    [block ~width:2 ~height:1 |> circle |> red]
*) 


(** [color c gen] is a generator similar to [gen], but with all block
    having color [c]. *)
val color : Gg.color -> t -> t

(** [shape s gen] is a generator similar to [gen], but with all block
   having shape [s] *)
val shape : Shape.t -> t -> t


(** Some predefined color combinators (more are available in
   Colors.Combinator).
*)

val black : t -> t
val red : t -> t
val blue : t -> t
val green : t -> t
val white : t -> t


(** Some predefined shape combinators *)
  
val solid : t -> t (** plain rectangle *)
val very_thin : t -> t (** outlined rectangle *)
val thin : t -> t (** outlined rectangle *)
val thick : t -> t  (** outlined rectangle *)
val circle : t -> t (** solid ellipsoidal *)
val square : t -> t (** solid rectangle *)
val bottom_left_triangle : t -> t (** solid square triangle*)
val bottom_right_triangle : t -> t (** solid square triangle*)
val top_left_triangle : t -> t (** solid square triangle*)
val top_right_triangle : t -> t (** solid square triangle*)
