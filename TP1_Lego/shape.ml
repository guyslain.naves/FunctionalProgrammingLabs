
type t = Vg.path

let circle =
  Vg.P.(circle (Gg.P2.v 0.5 0.5) 0.5 empty)

let square =
  Vg.P.(rect (Gg.Box2.v Gg.P2.o (Gg.P2.v 1. 1.)) empty)

let bottom_left_triangle =
  Vg.P.(empty 
        |> line Gg.V2.ox
        |> line Gg.V2.oy
        |> close)

let bottom_right_triangle = 
  Vg.P.(empty 
        |> line Gg.V2.ox
        |> line Gg.V2.(ox + oy)
        |> close)

let top_left_triangle =
  Vg.P.(empty
        |> line Gg.V2.(ox + oy)
        |> line Gg.V2.oy
        |> close)

let top_right_triangle =
  Vg.P.(empty
        |> sub Gg.V2.ox
        |> line Gg.V2.(ox + oy)
        |> line Gg.V2.oy
        |> close)

let outlined ~thinness =
  let eps = thinness and coeps = 1. -. thinness in
  Vg.P.(empty
        |> line Gg.V2.ox
        |> line Gg.V2.(ox + oy)
        |> line Gg.V2.oy
        |> close
        |> sub Gg.V2.(v eps eps)
        |> line Gg.V2.(v coeps eps)
        |> line Gg.V2.(v coeps coeps)
        |> line Gg.V2.(v eps coeps)
        |> close)



let solid = square 
let very_thin = outlined ~thinness:0.02
let thin = outlined ~thinness:0.05
let thick = outlined ~thinness:0.1
let very_thick = outlined ~thinness:0.2

let default = square
