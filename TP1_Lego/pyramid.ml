let sandy_colors =
  let open Colors in
  Combinator.gradient
    ~n:100
    Pure.tan
    Pure.gold

let brick =
  let open Generator in
  sample sandy_colors @@ fun color -> 
  block ~height:1 ~width:1
  |> color
    

let layer k =
  let open Generator in
  if k = 0 then
    brick
  else
    brick
    |> left_of (blue brick)
    |> pack_horizontally ~n:k
    |> left_of brick

let rec layer brick_nb =
  let open Generator in 
  if brick_nb = 1 then
    brick
  else
    brick |> left_of brick |> left_of (layer (brick_nb - 2))

let rec pyramid size =
  let open Generator in
  if size = 1 then
    layer 1
  else
    pyramid (size - 1)
    |> x_shift 1
    |> above (layer (2 * size - 1))

let small_pyramid = pyramid 2

let many_pyramids =
  let open Generator in
  small_pyramid
  |> pack_horizontally ~gap:2 ~n:10 

let large_view_params =
  let open Renderer in
  let open Gg in 
  { default_params with
    view = Box2.v P2.o V2.(v 100. 100.)
  }
  
let main =
  let open Renderer in
  generate_and_render
    ~params:large_view_params
    (pyramid 50)
