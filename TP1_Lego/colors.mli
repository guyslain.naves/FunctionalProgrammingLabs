(** Defines svg colors *)

(** Defines svg colors represented in [Gg.color] format. *)
module Pure :
sig

  (** [gradient ~n color1 color2] generates a linear gradient of [n]
      colors between two given colors. *)
  val gradient : n:int -> Gg.color -> Gg.color -> Gg.color list
  
  val aliceblue : Gg.color
  val antiquewhite : Gg.color
  val aqua : Gg.color
  val aquamarine : Gg.color
  val azure : Gg.color
  val beige : Gg.color
  val bisque : Gg.color
  val black : Gg.color
  val blanchedalmond : Gg.color
  val blue : Gg.color
  val blueviolet : Gg.color
  val brown : Gg.color
  val burlywood : Gg.color
  val cadetblue : Gg.color
  val chartreuse : Gg.color
  val chocolate : Gg.color
  val coral : Gg.color
  val cornflowerblue : Gg.color
  val cornsilk : Gg.color
  val crimson : Gg.color
  val cyan : Gg.color
  val darkblue : Gg.color
  val darkcyan : Gg.color
  val darkgoldenrod : Gg.color
  val darkgray : Gg.color
  val darkgreen : Gg.color
  val darkgrey : Gg.color
  val darkkhaki : Gg.color
  val darkmagenta : Gg.color
  val darkolivegreen : Gg.color
  val darkorange : Gg.color
  val darkorchid : Gg.color
  val darkred : Gg.color
  val darksalmon : Gg.color
  val darkseagreen : Gg.color
  val darkslateblue : Gg.color
  val darkslategray : Gg.color
  val darkslategrey : Gg.color
  val darkturquoise : Gg.color
  val darkviolet : Gg.color
  val deeppink : Gg.color
  val deepskyblue : Gg.color
  val dimgray : Gg.color
  val dimgrey : Gg.color
  val dodgerblue : Gg.color
  val firebrick : Gg.color
  val floralwhite : Gg.color
  val forestgreen : Gg.color
  val fuchsia : Gg.color
  val gainsboro : Gg.color
  val ghostwhite : Gg.color
  val gold : Gg.color
  val goldenrod : Gg.color
  val gray : Gg.color
  val grey : Gg.color
  val green : Gg.color
  val greenyellow : Gg.color
  val honeydew : Gg.color
  val hotpink : Gg.color
  val indianred : Gg.color
  val indigo : Gg.color
  val ivory : Gg.color
  val khaki : Gg.color
  val lavender : Gg.color
  val lavenderblush : Gg.color
  val lawngreen : Gg.color
  val lemonchiffon : Gg.color
  val lightblue : Gg.color
  val lightcoral : Gg.color
  val lightcyan : Gg.color
  val lightgoldenrodyellow : Gg.color
  val lightgray : Gg.color
  val lightgreen : Gg.color
  val lightgrey : Gg.color
  val lightpink : Gg.color
  val lightsalmon : Gg.color
  val lightseagreen : Gg.color
  val lightskyblue : Gg.color
  val lightslategray : Gg.color
  val lightslategrey : Gg.color
  val lightsteelblue : Gg.color
  val lightyellow : Gg.color
  val lime : Gg.color
  val limegreen : Gg.color
  val linen : Gg.color
  val magenta : Gg.color
  val maroon : Gg.color
  val mediumaquamarine : Gg.color
  val mediumblue : Gg.color
  val mediumorchid : Gg.color
  val mediumpurple : Gg.color
  val mediumseagreen : Gg.color
  val mediumslateblue : Gg.color
  val mediumspringgreen : Gg.color
  val mediumturquoise : Gg.color
  val mediumvioletred : Gg.color
  val midnightblue : Gg.color
  val mintcream : Gg.color
  val mistyrose : Gg.color
  val moccasin : Gg.color
  val navajowhite : Gg.color
  val navy : Gg.color
  val oldlace : Gg.color
  val olive : Gg.color
  val olivedrab : Gg.color
  val orange : Gg.color
  val orangered : Gg.color
  val orchid : Gg.color
  val palegoldenrod : Gg.color
  val palegreen : Gg.color
  val paleturquoise : Gg.color
  val palevioletred : Gg.color
  val papayawhip : Gg.color
  val peachpuff : Gg.color
  val peru : Gg.color
  val pink : Gg.color
  val plum : Gg.color
  val powderblue : Gg.color
  val purple : Gg.color
  val red : Gg.color
  val rosybrown : Gg.color
  val royalblue : Gg.color
  val saddlebrown : Gg.color
  val salmon : Gg.color
  val sandybrown : Gg.color
  val seagreen : Gg.color
  val seashell : Gg.color
  val sienna : Gg.color
  val silver : Gg.color
  val skyblue : Gg.color
  val slateblue : Gg.color
  val slategray : Gg.color
  val slategrey : Gg.color
  val snow : Gg.color
  val springgreen : Gg.color
  val steelblue : Gg.color
  val tan : Gg.color
  val teal : Gg.color
  val thistle : Gg.color
  val tomato : Gg.color
  val turquoise : Gg.color
  val violet : Gg.color
  val wheat : Gg.color
  val white : Gg.color
  val whitesmoke : Gg.color
  val yellow : Gg.color
  val yellowgreen : Gg.color
end


(** Defines svg colors in term of combinators that can be applied on
    block generators. *)
module Combinator :
sig


  (** [gradient ~n color1 color2] generates a list of [n] combinators
      for coloring with a gradient of colors from [color1] to [color2]. *)
  val gradient :
    n:int -> Gg.color -> Gg.color
    -> (Generator.t -> Generator.t) list

  
  val aliceblue : Generator.t -> Generator.t
  val antiquewhite : Generator.t -> Generator.t
  val aqua : Generator.t -> Generator.t
  val aquamarine : Generator.t -> Generator.t
  val azure : Generator.t -> Generator.t
  val beige : Generator.t -> Generator.t
  val bisque : Generator.t -> Generator.t
  val black : Generator.t -> Generator.t
  val blanchedalmond : Generator.t -> Generator.t
  val blue : Generator.t -> Generator.t
  val blueviolet : Generator.t -> Generator.t
  val brown : Generator.t -> Generator.t
  val burlywood : Generator.t -> Generator.t
  val cadetblue : Generator.t -> Generator.t
  val chartreuse : Generator.t -> Generator.t
  val chocolate : Generator.t -> Generator.t
  val coral : Generator.t -> Generator.t
  val cornflowerblue : Generator.t -> Generator.t
  val cornsilk : Generator.t -> Generator.t
  val crimson : Generator.t -> Generator.t
  val cyan : Generator.t -> Generator.t
  val darkblue : Generator.t -> Generator.t
  val darkcyan : Generator.t -> Generator.t
  val darkgoldenrod : Generator.t -> Generator.t
  val darkgray : Generator.t -> Generator.t
  val darkgreen : Generator.t -> Generator.t
  val darkgrey : Generator.t -> Generator.t
  val darkkhaki : Generator.t -> Generator.t
  val darkmagenta : Generator.t -> Generator.t
  val darkolivegreen : Generator.t -> Generator.t
  val darkorange : Generator.t -> Generator.t
  val darkorchid : Generator.t -> Generator.t
  val darkred : Generator.t -> Generator.t
  val darksalmon : Generator.t -> Generator.t
  val darkseagreen : Generator.t -> Generator.t
  val darkslateblue : Generator.t -> Generator.t
  val darkslategray : Generator.t -> Generator.t
  val darkslategrey : Generator.t -> Generator.t
  val darkturquoise : Generator.t -> Generator.t
  val darkviolet : Generator.t -> Generator.t
  val deeppink : Generator.t -> Generator.t
  val deepskyblue : Generator.t -> Generator.t
  val dimgray : Generator.t -> Generator.t
  val dimgrey : Generator.t -> Generator.t
  val dodgerblue : Generator.t -> Generator.t
  val firebrick : Generator.t -> Generator.t
  val floralwhite : Generator.t -> Generator.t
  val forestgreen : Generator.t -> Generator.t
  val fuchsia : Generator.t -> Generator.t
  val gainsboro : Generator.t -> Generator.t
  val ghostwhite : Generator.t -> Generator.t
  val gold : Generator.t -> Generator.t
  val goldenrod : Generator.t -> Generator.t
  val gray : Generator.t -> Generator.t
  val grey : Generator.t -> Generator.t
  val green : Generator.t -> Generator.t
  val greenyellow : Generator.t -> Generator.t
  val honeydew : Generator.t -> Generator.t
  val hotpink : Generator.t -> Generator.t
  val indianred : Generator.t -> Generator.t
  val indigo : Generator.t -> Generator.t
  val ivory : Generator.t -> Generator.t
  val khaki : Generator.t -> Generator.t
  val lavender : Generator.t -> Generator.t
  val lavenderblush : Generator.t -> Generator.t
  val lawngreen : Generator.t -> Generator.t
  val lemonchiffon : Generator.t -> Generator.t
  val lightblue : Generator.t -> Generator.t
  val lightcoral : Generator.t -> Generator.t
  val lightcyan : Generator.t -> Generator.t
  val lightgoldenrodyellow : Generator.t -> Generator.t
  val lightgray : Generator.t -> Generator.t
  val lightgreen : Generator.t -> Generator.t
  val lightgrey : Generator.t -> Generator.t
  val lightpink : Generator.t -> Generator.t
  val lightsalmon : Generator.t -> Generator.t
  val lightseagreen : Generator.t -> Generator.t
  val lightskyblue : Generator.t -> Generator.t
  val lightslategray : Generator.t -> Generator.t
  val lightslategrey : Generator.t -> Generator.t
  val lightsteelblue : Generator.t -> Generator.t
  val lightyellow : Generator.t -> Generator.t
  val lime : Generator.t -> Generator.t
  val limegreen : Generator.t -> Generator.t
  val linen : Generator.t -> Generator.t
  val magenta : Generator.t -> Generator.t
  val maroon : Generator.t -> Generator.t
  val mediumaquamarine : Generator.t -> Generator.t
  val mediumblue : Generator.t -> Generator.t
  val mediumorchid : Generator.t -> Generator.t
  val mediumpurple : Generator.t -> Generator.t
  val mediumseagreen : Generator.t -> Generator.t
  val mediumslateblue : Generator.t -> Generator.t
  val mediumspringgreen : Generator.t -> Generator.t
  val mediumturquoise : Generator.t -> Generator.t
  val mediumvioletred : Generator.t -> Generator.t
  val midnightblue : Generator.t -> Generator.t
  val mintcream : Generator.t -> Generator.t
  val mistyrose : Generator.t -> Generator.t
  val moccasin : Generator.t -> Generator.t
  val navajowhite : Generator.t -> Generator.t
  val navy : Generator.t -> Generator.t
  val oldlace : Generator.t -> Generator.t
  val olive : Generator.t -> Generator.t
  val olivedrab : Generator.t -> Generator.t
  val orange : Generator.t -> Generator.t
  val orangered : Generator.t -> Generator.t
  val orchid : Generator.t -> Generator.t
  val palegoldenrod : Generator.t -> Generator.t
  val palegreen : Generator.t -> Generator.t
  val paleturquoise : Generator.t -> Generator.t
  val palevioletred : Generator.t -> Generator.t
  val papayawhip : Generator.t -> Generator.t
  val peachpuff : Generator.t -> Generator.t
  val peru : Generator.t -> Generator.t
  val pink : Generator.t -> Generator.t
  val plum : Generator.t -> Generator.t
  val powderblue : Generator.t -> Generator.t
  val purple : Generator.t -> Generator.t
  val red : Generator.t -> Generator.t
  val rosybrown : Generator.t -> Generator.t
  val royalblue : Generator.t -> Generator.t
  val saddlebrown : Generator.t -> Generator.t
  val salmon : Generator.t -> Generator.t
  val sandybrown : Generator.t -> Generator.t
  val seagreen : Generator.t -> Generator.t
  val seashell : Generator.t -> Generator.t
  val sienna : Generator.t -> Generator.t
  val silver : Generator.t -> Generator.t
  val skyblue : Generator.t -> Generator.t
  val slateblue : Generator.t -> Generator.t
  val slategray : Generator.t -> Generator.t
  val slategrey : Generator.t -> Generator.t
  val snow : Generator.t -> Generator.t
  val springgreen : Generator.t -> Generator.t
  val steelblue : Generator.t -> Generator.t
  val tan : Generator.t -> Generator.t
  val teal : Generator.t -> Generator.t
  val thistle : Generator.t -> Generator.t
  val tomato : Generator.t -> Generator.t
  val turquoise : Generator.t -> Generator.t
  val violet : Generator.t -> Generator.t
  val wheat : Generator.t -> Generator.t
  val white : Generator.t -> Generator.t
  val whitesmoke : Generator.t -> Generator.t
  val yellow : Generator.t -> Generator.t
  val yellowgreen : Generator.t -> Generator.t
end
