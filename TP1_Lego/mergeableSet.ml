type 'elt node =
  { size : int;
    left : 'elt t;
    root : 'elt;
    right : 'elt t
  } 
and 'elt t =
  | Leaf
  | Node of 'elt node


let size = function
  | Leaf -> 0
  | Node node -> node.size


let node left root right =
  Node { size = 1 + size left + size right;
         left;
         root;
         right
       }


let empty = Leaf
let is_empty set = set = Leaf

let singleton elt =
  Node { size = 1; left = Leaf; root = elt; right = Leaf }


let rec merge set1 set2 =
  let (set1,set2) =
    if size set1 > size set2 then (set1,set2) else (set2, set1)
  in 
  match set1, set2 with
  | Leaf, _ -> set2
  | _, Leaf -> set1
  | Node node1, Node node2 when size node1.left > size node1.right ->
    node node1.left node1.root (merge node1.right set2)
  | Node node1, Node node2 ->
    node (merge node1.left set2) node1.root node1.right


let add elt set =
  merge (singleton elt) set


(* let rec map ~f = function *)
(*   | Leaf -> Leaf *)
(*   | Node node -> *)
(*     Node { size = node.size; *)
(*            left = map ~f node.left; *)
(*            root = f node.root; *)
(*            right = map ~f node.right *)
(*          } *)


let rec map_k ~f tree k = (* CPS style *)
  match tree with 
  | Leaf -> k Leaf
  | Node node ->
    map_k ~f node.left @@ fun mapped_left ->
    f node.root |> fun mapped_root ->
    map_k ~f node.right @@ fun mapped_right ->
    Node { size = node.size;
           left = mapped_left;
           root = mapped_root;
           right = mapped_right
         } |>
    k
    
let map ~f tree = map_k ~f tree (fun res -> res)

(* let rec catamorphism ~at_leaf ~on_node = function *)
(*   | Leaf -> at_leaf *)
(*   | Node node -> *)
(*     on_node *)
(*       (catamorphism ~at_leaf ~on_node node.left) *)
(*       node.root *)
(*       (catamorphism ~at_leaf ~on_node node.right) *)


let rec catamorphism_k ~at_leaf ~on_node tree k = (* CPS style *)
  match tree with
  | Leaf -> k at_leaf
  | Node node ->
    catamorphism_k ~at_leaf ~on_node node.left @@ fun left_res ->
    catamorphism_k ~at_leaf ~on_node node.right @@ fun right_res ->
    on_node left_res node.root right_res |>
    k

let catamorphism ~at_leaf ~on_node tree =
  catamorphism_k ~at_leaf ~on_node tree (fun res -> res)
    
(* let rec fold ~f init = function *)
(*   | Leaf -> init *)
(*   | Node node -> *)
(*     fold ~f init node.left *)
(*     |> fun state -> f state node.root *)
(*     |> fun state -> fold ~f state node.right *)


let rec fold_k ~f init tree k =
  match tree with
  | Leaf -> k init
  | Node node ->
    fold_k ~f init node.left @@ fun left_res ->
    f left_res node.root |> fun middle_res ->
    fold_k ~f middle_res node.right @@ fun right_res ->
    k right_res

let fold ~f init tree =
  fold_k ~f init tree (fun res -> res)




(* let rec traverse ~f set state = *)
(*   match set with *)
(*   | Leaf -> (Leaf, state) *)
(*   | Node node -> *)
(*     traverse ~f node.left state |> fun (left,state) -> *)
(*     f node.root state |> fun (root,state) -> *)
(*     traverse ~f node.right state |> fun (right, state) -> *)
(*     (Node { node with left; root; right }, state) *)


let rec traverse_k ~f set state k =
  match set with
  | Leaf -> k (Leaf, state)
  | Node node ->
    traverse_k ~f node.left state @@ fun (left, state) ->
    f node.root state |> fun (root, state) ->
    traverse_k ~f node.right state @@ fun (right,state) ->
    k (Node { node with left; root; right }, state)

let traverse ~f set state =
  traverse_k ~f set state (fun res -> res)


(* let rec for_each ~f = function *)
(*   | Leaf -> () *)
(*   | Node node -> *)
(*     for_each ~f node.left; *)
(*     f node.root; *)
(*     for_each ~f node.right *)

let rec for_each_k ~f tree k =
  match tree with
  | Leaf -> k ()
  | Node node ->
    for_each_k ~f node.left @@ fun () ->
    f node.root;
    for_each_k ~f node.right @@
    k

let for_each ~f tree = for_each_k ~f tree (fun () -> ())
