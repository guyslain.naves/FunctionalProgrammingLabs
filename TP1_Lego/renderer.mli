(** Turns generators to images. 

    Images are saved in a file in svg format. 
*)


(** Rendering is parametrized using this type. *)
type params =
  { size : Gg.size2;
    (** The size of the image generated in mm. *)
    view : Gg.box2;
    (** The part of the plane that is rendered in the image. *)
    file_name : string
    (** The name to give to the file in which the image is saved. *)
  }


(** Some default parameters. *)
val default_params : params


(** Render a component into an image. *)
val render :
  ?params:params -> Component.t -> unit

(** Render many components, in order from foreground to background,
    into a single image. *)
val render_many :
  ?params:params -> Component.t list -> unit

(** Generates a component and render it into an image. *)
val generate_and_render : ?params:params -> Generator.t -> unit
