(** A set of bricks.

    Apart from the type, the functions in this module should generally
    not be used, as most of the work should be done using the module
    [Generator].
*)


(** The type for sets of bricks *)
type t
  
val block : width:int -> height:int -> t

val merge : t -> t -> t

val blocks : t -> Block.t MergeableSet.t


val horizontal_gap : left:t -> right:t -> int
val vertical_gap : below:t -> above:t -> int 
 
val traverse :
  f:(Block.t -> 'state -> (Block.t * 'state))
  -> t -> 'state -> t * 'state


val x_shift : int -> t -> t
val y_shift : int -> t -> t
val move : (int * int) -> t -> t


val map : f:(Block.t -> Block.t) -> t -> t

val traverse :
  f:(Block.t -> 'state -> (Block.t * 'state))
  -> t -> 'state -> (t * 'state)

val for_each :
  f:(Block.t -> unit)
  -> t -> unit

(* For debugging purpose only *)
(* val show : t -> string *)
