type params =
  { size : Gg.size2;
    view : Gg.box2;
    file_name : string
  }
             
let default_params =
  { size = Gg.V2.v 100. 100.;
    view = Gg.Box2.v Gg.V2.zero (Gg.V2.v 20. 20.);
    file_name = "out.svg";
  }

let area = `Anz
  
let img_of_block a_block =
  let open Block in
  let origin =
    Gg.V2.v (float (fst a_block.position)) (float (snd a_block.position))
  in
  let diagonal =
    Gg.V2.v (float a_block.width) (float a_block.height)
  in
  let path = a_block.shape in
  Vg.I.const a_block.color
  |> Vg.I.cut ~area path
  |> Vg.I.scale diagonal
  |> Vg.I.move origin

let transparent_image =
  Vg.I.const Gg.Color.(with_a white 0.)

let img_of_component component =
  MergeableSet.fold
    ~f:(fun img block -> Vg.I.blend (img_of_block block) img)
    transparent_image
    (Component.blocks component)

let img_of_components components =
  List.fold_left
    (fun img comp -> Vg.I.blend (img_of_component comp) img)
    transparent_image
    components
    

let to_file file_name renderable =
  let file_out = open_out file_name in
  let renderer = Vg.Vgr.create (Vgr_svg.target ()) (`Channel file_out) in
  ignore @@ Vg.Vgr.render renderer renderable;
  ignore @@ Vg.Vgr.render renderer `End;
  close_out file_out

let render_img ~params img = 
  let renderable = `Image (params.size,params.view,img) in
  to_file params.file_name renderable

let render ?(params=default_params) component =
  component
  |> img_of_component
  |> render_img ~params

let render_many ?(params=default_params) components =
  components
  |> img_of_components
  |> render_img ~params


let generate_and_render ?params generator = 
  generator
  |> Generator.generate (Random.get_state ())
  |> fst
  |> render ?params
