let colors =
  let open Colors in
  Combinator.gradient
    ~n:100
    Pure.aquamarine
    Pure.deepskyblue

let brick =
  let open Generator in
  sample colors @@ fun color ->
  color (block ~width:1 ~height:1)


let rec power base exp =
  if exp = 0 then 1
  else if exp mod 2 == 0 then power (base * base) (exp / 2)
  else base * power (base * base) (exp / 2)

let rec cube n =
  let open Generator in 
  if n = 0 then
    brick
  else
    let gap = power 3 (n-1) in 
    let sub = cube (n-1) in
    (sub |> left_of sub |> left_of sub)
    |> above (sub |> left_of ~gap sub)
    |> above (sub |> left_of sub |> left_of sub)


let params =
  let open Renderer in
  let open Gg in 
  { default_params with
    view =
      Box2.v 
        (V2.v 0. 0.)
        (V2.v 81. 81.)
  }

let main =
  let open Renderer in
  generate_and_render
    ~params
    (cube 4)
