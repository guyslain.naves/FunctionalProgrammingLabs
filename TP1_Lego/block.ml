
type t = {
  position : (int * int);
  width : int;
  height : int;
  color : Gg.color;
  shape : Shape.t 
}

let color color brick = { brick with color }

let block ~width ~height =
  { position = (0,0);
    width;
    height;
    color = Gg.Color.black;
    shape = Shape.default
  }

let unit = block ~width:1 ~height:1

let move (dx,dy) brick =
  let (x,y) = brick.position in
  { brick with position = (x + dx, y + dy) }

let black = color Gg.Color.black
let red = color Gg.Color.red
let blue = color Gg.Color.blue
let green = color Gg.Color.green
let white = color Gg.Color.white

let shape shape brick = { brick with shape }

let solid = shape Shape.default
let very_thin = shape Shape.very_thin 
let thin = shape Shape.thin
let thick = shape Shape.thick

let circle = shape Shape.circle
let square = shape Shape.square
let bottom_left_triangle = shape Shape.bottom_left_triangle
let bottom_right_triangle = shape Shape.bottom_right_triangle
let top_left_triangle = shape Shape.top_left_triangle
let top_right_triangle = shape Shape.top_right_triangle 
