module Client = Cohttp_lwt_unix.Client
module Body = Cohttp_lwt_body


let create_query_body xml_query =
  Xml.pp Format.str_formatter xml_query;
  Format.flush_str_formatter () |> Body.of_string



exception Response_error of Cohttp.Code.status_code 
let get_response_body (response, body) : Cohttp_lwt_body.t Lwt.t =
  match Cohttp.Response.(response.status) with
  | `OK -> Lwt.return body
  | error -> Lwt.fail (Response_error error)


let write_into_file ~filename body : unit Lwt.t =
  let open Lwt in 
  Lwt_io.(open_file ~mode:Output) filename >>= fun out_channel ->
  Body.write_body (Lwt_io.write out_channel) body >>= fun () ->
  Lwt_io.close out_channel


let fetch ~to_file:filename uri ~body =
  let open Lwt in
  Client.post ~body uri >>=
  get_response_body >>=
  write_into_file ~filename





