module Option = Core.Std.Option
module List = Core.Std.List
module IntMap = Osm.IntMap



let create_node_map json =
  let open Yojson.Basic.Util in
  json
  |> member "elements"
  |> to_list
  |> List.map ~f:Osm.Node.of_json
  |> List.filter_opt 
  |> List.fold_left
    ~f:(fun map node -> IntMap.add Osm.Node.(node.id) node map)
    ~init:IntMap.empty

let create_way_list nodes json =
  let open Yojson.Basic.Util in
  json
  |> member "elements"
  |> to_list
  |> List.map ~f:(Osm.Way.of_json nodes)
  |> List.filter_opt
       



let json = Yojson.Basic.from_file "marseille.json"
let nodes = create_node_map json
let ways = create_way_list nodes json 

let nbr_nodes = IntMap.cardinal nodes
let nbr_ways = List.length ways


let list_consecutives list =
  let rec loop pairs = function
    | one::(two::others as tail) -> loop ((one,two)::pairs) tail
    | _ -> List.rev pairs
  in
  loop [] list 

let way_length way =
  Osm.Way.(way.nodes)
  |> List.map ~f:Osm.Node.to_vector
  |> list_consecutives
  |> List.map ~f:(fun (v1,v2) -> Gg.V3.(norm (v2 - v1)))
  |> List.fold ~init:0. ~f:(+.)
  

let total_length =
  ways
  |> List.map ~f:way_length
  |> List.fold ~init:0. ~f:(+.)


let main =
  Printf.printf "Length of roads in Marseille: %.1fkm\n%!" (total_length *. 1e-3)
