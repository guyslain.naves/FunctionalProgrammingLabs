val create_query_body : Xml.t -> Cohttp_lwt_body.t

exception Response_error of Cohttp.Code.status_code

val get_response_body :
  (Cohttp.Response.t * Cohttp_lwt_body.t) -> Cohttp_lwt_body.t Lwt.t 


val write_into_file :
  filename:string -> Cohttp_lwt_body.t -> unit Lwt.t

val fetch :
  to_file:string -> Uri.t -> body:Cohttp_lwt_body.t -> unit Lwt.t 
