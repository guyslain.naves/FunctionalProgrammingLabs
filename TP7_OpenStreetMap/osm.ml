module Option = Core.Std.Option
module List = Core.Std.List
module IntMap = Map.Make(struct type t = int let compare a b = a - b end)


module Node =
struct
  
  type id = int 
  type t =
    { id : id;
      longitude : float;
      lattitude : float;
      tags : (string * Yojson.Basic.json) list
    }

  let compare { id = id1 } { id = id2 } = id1 - id2


  let to_vector { longitude; lattitude } =
    let earth_radius = 6.371e6 in
    let azimuthal_angle = longitude *. Gg.Float.pi /. 180. in
    let polar_angle = (90. -. lattitude) *. Gg.Float.pi /. 180. in 
    Gg.V3.v earth_radius azimuthal_angle polar_angle
    |> Gg.V3.of_spherical


  let of_json json : t option = 
    match json with
    | `Assoc assoc when List.Assoc.find assoc "type" = Some (`String "node")->
      let open Yojson.Basic.Util in
      Some {
        id = json |> member "id" |> to_int;
        lattitude = json |> member "lat" |> to_float;
        longitude = json |> member "lon" |> to_float;
        tags = List.([json] |> filter_member "tags" >>= to_assoc)
      }
    | otherwise -> None

 
end

module Way =
struct

  type id = int 
  type t =
    { id : int;
      nodes : Node.t list;
      tags : (string * Yojson.Basic.json) list
    }

  let compare { id = id1 } { id = id2 } = id1 - id2

  let node_of_id nodes id =
    if IntMap.mem id nodes then Some (IntMap.find id nodes)
    else None

  let nodes_of_ids nodes ids =
    ids
    |> Yojson.Basic.Util.to_list
    |> List.map ~f:Yojson.Basic.Util.to_int
    |> List.map ~f:(node_of_id nodes)
    |> List.filter_opt
    
  
  let of_json nodes json : t option =
    match json with
    | `Assoc list when List.Assoc.find list "type" = Some (`String "way") ->
      let open Yojson.Basic.Util in
      Some {
        id = json |> member "id" |> to_int;
        nodes = json |> member "nodes" |> nodes_of_ids nodes;
        tags = List.([json] |> filter_member "tags" >>= to_assoc)
      }
    | _ -> None 
      
end
