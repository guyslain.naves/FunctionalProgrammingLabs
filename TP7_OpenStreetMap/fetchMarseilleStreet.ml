
let osm_overpass_uri =
  Uri.of_string "http://overpass-api.de/api/interpreter"

let marseille_streets_query =
  let open Xml in 
  element "osm-script" ~attributes:["output" ==> "json"] [
    element "union" [
      element "query" ~attributes:["type" ==> "way"] [
        element "has-kv" ~attributes:["k" ==> "highway"] [];
        element "bbox-query"
          ~attributes:["s" ==> "43.19";
                       "n" ==> "43.39";
                       "w" ==> "5.268";
                       "e" ==> "5.51"] []
      ];
      element "recurse" ~attributes:["type" ==> "way-node"] [];
    ];
    element "print" []
  ]



let main =
  let body =
    FetchPostXml.create_query_body marseille_streets_query
  in
  FetchPostXml.fetch osm_overpass_uri ~body ~to_file:"marseille.json"
  |> Lwt_main.run
  
