type tag = string 
type attribute =
  { key : string;
    value : string
  }

type element =
  { name : string;
    attributes : attribute list;
    children : t list
  }

and t =
  | Element of element
  | Data of string


let (==>) key value = { key; value }

let element name ?(attributes=[]) children =
  Element { name; attributes; children}

let data text = Data text

let pp_attribute ppf {key; value} =
  Format.fprintf ppf "@[%s=\"%s\"@]" key value

(* Format.pp_print_list since OCaml 4.02 *)
let format_pp_print_list ?(pp_sep=Format.pp_print_cut) pp_elt ppf list =
  let rec loop ppf = function
    | [] -> ()
    | [single] -> pp_elt ppf single
    | first::others ->
      Format.fprintf ppf "%a%a%a"
        pp_elt first
        pp_sep ()
        loop others
  in
  loop ppf list 

let pp_attributes ppf = function
  | [] -> ()
  | list -> 
    let pp_sep ppf () = Format.pp_print_break ppf 1 0 in 
    Format.fprintf ppf "@ %a"
      (format_pp_print_list ~pp_sep pp_attribute) list 

let pp_start_tag ?(stop=false) ppf { name; attributes } =
  Format.fprintf ppf "@[%s@[<hv 2>%s%a@]%s@]"
    "<"
    name
    pp_attributes attributes
    (if stop then "/>" else ">")

let pp_end_tag ppf { name } =
  Format.fprintf ppf "@[%s/%s>@]" "<" name



let rec pp ppf = function
  | Data text ->
    Format.fprintf ppf "@[%s@]" text
  | Element elt when elt.children = [] ->
    pp_start_tag ~stop:true ppf elt
  | Element elt ->
    Format.fprintf ppf "@[<v 0>%a@;<0 2>@[<v 0>%a@]@;<0 0>%a@]"
      (pp_start_tag ~stop:false) elt
      pp_children elt.children
      pp_end_tag elt

and pp_children ppf children =
  format_pp_print_list
    ~pp_sep:(fun ppf () -> Format.pp_print_break ppf 1 0)
    pp 
    ppf
    children
