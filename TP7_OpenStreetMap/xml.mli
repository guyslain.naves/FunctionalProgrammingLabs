type tag = string (** name of a tag (a node) *)
type attribute (** an attribute that can be associated to a node *)
type t (** an xml tree *)

(** [key ==> value] is an attribute where [key] is associated to [value]. *)
val (==>) : string -> string -> attribute

(** [element name ~attributes children] is a node with 
    tag name [name], attributes [attributes] or [[]] by default,
    and children [children].
*)
val element : tag -> ?attributes:attribute list -> t list -> t

(** [data text] is a data element made of some string [text] *)
val data : string -> t

val pp : Format.formatter -> t -> unit
