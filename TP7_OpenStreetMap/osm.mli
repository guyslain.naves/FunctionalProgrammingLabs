module IntMap : Map.S with type key = int 

module Node : sig
  
  type id = int 
  type t =
    { id : id;
      longitude : float;
      lattitude : float;
      tags : (string * Yojson.Basic.json) list
    }

  val compare : t -> t -> int 

  val to_vector : t -> Gg.v3 

  val of_json : Yojson.Basic.json -> t option
      
end


module Way : sig
  
  type id = int 
  type t =
    { id : id;
      nodes : Node.t list;
      tags : (string * Yojson.Basic.json) list
    }

  val compare : t -> t -> int

  val of_json : Node.t IntMap.t -> Yojson.Basic.json -> t option
      
end
