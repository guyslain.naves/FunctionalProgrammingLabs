#!/home/guyslain/.opam/4.03.0/bin/ocaml
(* add #!/path/to/ocaml as first line, and do
chmod +x on the file to make it an executable
*)

(* There is an error in OCaml 4.03 and 4.04.0 that 
   makes so that Ephemeron is not loaded by the interpreter,
   but Ephemeron is used by Core so we must load it. 

   A workaround is to load stdlib.cma, but this has the bad effect of 
   somehow reinitializing Sys.argv, with arguments:
   0 ocaml
   1 the name of the script
   2 the first argument for the script, and so on. 

   Only in 4.03 and 4.04.0 we have to take a dummy argument,
   the name of the script. On any other version of ocaml, remove
   the load stdlib.cma line and the scriptname anonymous argument
   in the spec of the command-line (at the end of the script).
*)


#load "stdlib.cma";; (* only on ocaml 4.03.0 and 4.04.0 *)

#use "topfind";;
#thread;;
#camlp4o;;
#require "core";;

(* If args are wrong, check them with this: 
let () =
  Printf.printf "args:\n";
  Array.iter (Printf.printf "%s\n") Sys.argv;
  flush stdout
*)


open Core


let ignore_directory = [ "_darcs"; "_build" ]

module Stats =
struct

  type t =
    {
      lines_of_code : int;
      bytes : int;
      files : int;
    }

  let zero =
    { lines_of_code = 0;
      bytes = 0;
      files = 0;
    }

  let nbr_of_lines_of_file filename =
    let in_chan = In_channel.create filename in
    let lines = In_channel.input_lines in_chan in
    let () = In_channel.close in_chan in
    List.length lines

  let size_of_file filename =
    let open Unix in 
    let file_stats = Unix.stat filename in
    file_stats.st_size
    |> Int64.to_int
    |> Option.value ~default:0

  let of_file filename =
    { lines_of_code = nbr_of_lines_of_file filename;
      bytes = size_of_file filename;
      files = 1;
    }

  let add stats1 stats2 =
    { lines_of_code = stats1.lines_of_code + stats2.lines_of_code;
      bytes = stats1.bytes + stats2.bytes;
      files = stats1.files + stats2.files;
    }

  let to_string stats =
    Printf.sprintf "Lines: %d, Bytes: %d, Files: %d"
      stats.lines_of_code
      stats.bytes
      stats.files

  
  let to_strings stats =
    [ string_of_int stats.lines_of_code;
      string_of_int stats.bytes;
      string_of_int stats.files
    ]
    
  let header =
    [ "Lines of code";
      "Size (bytes)";
      "Files"
    ]
  
end

module Kind =
struct
  type t =
    { language : string;
      extension : string
    }

  let extensions =
    [ ("ml","Ocaml");
      ("mli","Ocaml");
      ("java","java");
      ("c","C");
      ("h","C");
      ("hs","haskell");
      ("hi","haskell");
      ("cpp","C++");
      ("hpp","C++")
    ]

  let of_extension extension =
    List.Assoc.find ~equal:String.equal extensions extension 
    |> Option.map ~f:(fun language -> { language; extension })
  
  let of_file filename =
    let (basename,extension) = Filename.split_extension filename in
    Option.bind extension  of_extension
  
  let compare_language kind1 kind2 =
    String.compare kind1.language kind2.language

  let compare kind1 kind2 =
    let delta = compare_language kind1 kind2 in
    if delta = 0 then String.compare kind1.extension kind2.extension
    else delta

  let equal kind1 kind2 = compare kind1 kind2 = 0

  let to_strings kind = [ kind.language; kind.extension ]
  let header = [ "Language"; "Extension" ]
      
end


module Digest =
struct
  type digest = (Kind.t * Stats.t) list 

  let empty = []

  let add digest (kind, stats) =
    List.Assoc.find ~equal:Kind.equal digest kind
    |> Option.map ~f:(Stats.add stats)
    |> Option.value ~default:stats
    |> List.Assoc.add ~equal:Kind.equal digest kind

  let merge digest1 digest2 =
    List.fold
      ~init:digest2
      ~f:add
      digest1
    
  let of_file filename =
    Kind.of_file filename
    |> Option.map ~f:(fun kind -> [(kind, Stats.of_file filename)])
    |> Option.value ~default:[]

  
  let item_to_string (kind,stats) =
    Printf.sprintf "Language: %s, extension: %s, %s"
      kind.Kind.language
      kind.Kind.extension
      (Stats.to_string stats)

  let compare_item (kind1,stats1) (kind2,stats2) =
    Kind.compare kind1 kind2
      
  let to_string digest =
    digest
    |> List.sort ~cmp:compare_item
    |> List.map ~f:item_to_string
    |> String.concat ~sep:"\n"

  let to_strings_list digest =
    List.map
      ~f:(fun (kind,stats) -> Kind.to_strings kind @ Stats.to_strings stats)
      digest

  let header =
    Kind.header @ Stats.header
end


module ArrayDisplay =
struct
  
  let compute_widths_of_line line =
    List.map ~f:String.length line

  let max_widths line1 line2 =
    List.map2_exn ~f:max line1 line2 

  let compute_widths lines =
    lines
    |> List.map ~f:compute_widths_of_line
    |> List.reduce ~f:max_widths
    |> Option.value ~default:[]

  let pad width string =
    string ^ String.make (width - String.length string) ' '
      
  let format ~widths items =
    List.map2_exn ~f:pad widths items
    |> String.concat ~sep:" | " 

  
  let display header lines =
    let column_widths = compute_widths (header :: lines) in
    let width =
      List.fold ~f:(+) ~init:0 column_widths + 3 * (List.length header - 1)
    in
    let formatted_header =
      format ~widths:column_widths header
    in
    let formatted_lines =
      List.map ~f:(format ~widths:column_widths) lines
    in
    let sepline = String.make width '-' in
    List.iter ~f:(fun line -> Printf.printf "%s\n" line)
      ( formatted_header ::
        sepline ::
        formatted_lines
      )
end
  



let is_source_file filename =
  let (basename, maybe_extension) = Filename.split_extension filename in
  let is_source_extension = List.Assoc.mem ~equal:String.equal Kind.extensions in 
  Option.exists maybe_extension ~f:is_source_extension

let is_valid_file full_name base_name =
  Sys.is_file full_name = `Yes
  && is_source_file base_name



let is_directory_hidden base_name= String.get base_name 0 = '.'

let is_valid_directory full_name base_name =
  Sys.is_directory full_name = `Yes
  && not (is_directory_hidden base_name) 
  && not (List.mem ~equal:String.equal ignore_directory base_name)



let rec digest_of_dir_item dir_name item_name =
  let full_name = Filename.concat dir_name item_name in
  if is_valid_directory full_name item_name then digest_of_dir full_name
  else if is_valid_file full_name item_name then Digest.of_file full_name
  else Digest.empty

and digest_of_dir dir_name =
  let open Option in 
  dir_name
  |> Sys.ls_dir
  |> List.map ~f:(digest_of_dir_item dir_name)
  |> List.fold_left ~f:Digest.merge ~init:Digest.empty



let show_dir_size dir_name =
  let digest = digest_of_dir dir_name in
  ArrayDisplay.display
    Digest.header
    (Digest.to_strings_list digest)


let count_sources_command =
  let cwd = Sys.getcwd () in
  let spec =
    Command.Spec.(
      empty
      +> anon ("scriptname" %: string) (* only on ocaml 4.03.0 and 4.04.0 *)
      +> anon (maybe_with_default cwd ("dirname" %: string))
    )
  in
  Command.basic
    ~summary:"Count size of source files in a directory recursively"
    spec
    (fun scriptname dirname () -> show_dir_size dirname)
    (* remove scriptname in previous line if not on 4.03 or 4.04.0 *)

let main =
  Command.run ~version:"0.6" count_sources_command
