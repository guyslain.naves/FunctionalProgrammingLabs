type 'elt t

(** Construction *)

(** [const e = [e; e; ...]] *)
val const : 'elt -> 'elt t

(** [iterate e ~f = [e; f e; f (f e); ...]] *) 
val iterate : 'elt -> f:('elt -> 'elt) -> 'elt t

(** [unfold state0 ~f = [ e0; e1; e2; ...]]
    where [f state_i = (e_i,state_(i+1))]
*)
val unfold : 'state -> f:('state -> ('state * 'elt)) -> 'elt t



(** Observation *)

(** observe the head and tail of an infinite list *)
val observe : 'elt t -> ('elt * 'elt t)


(** Manipulation *)

val append : 'elt -> 'elt t -> 'elt t 

(** [take k list] is the list of the first [k] elements of [list] *)
val take : int -> 'elt t -> 'elt list

(** [drop k list] is the list with the first [k] elements removed from [list] *)
val drop : int -> 'elt t -> 'elt t

val zip : 'left t -> 'right t -> ('left * 'right) t 
    

(** Higher-order functions *)

val map : 'elt t -> f:('elt -> 'im) -> 'im t

val filter : 'elt t -> f:('elt -> bool) -> 'elt t

(** [scan ~f ~init:state_0 [ e0; e1; e2; ...]] is
   [state_0; state_1; ...] where [state_(i+1) = f state_i e_i]
*)
val scan : f:('state -> 'elt -> 'state) -> init:'state -> 'elt t -> 'state t 
