module M =
struct

  type symbol = O | I 
  type state = A | B | C | D | Halt 

  let transition state symbol =
    match state, symbol with
    | A, O -> Some (I, `Right, B) 
    | A, I -> Some (I, `Left, B) 
    | B, O -> Some (I, `Left, A) 
    | B, I -> Some (O, `Left, C) 
    | C, O -> Some (I, `Right, Halt) 
    | C, I -> Some (I, `Left, D) 
    | D, O -> Some (I, `Right, D) 
    | D, I -> Some (O, `Right, A)
    | Halt, _ -> None

  let pp_symbol ppf = function
    | O -> Format.pp_print_string ppf "O"
    | I -> Format.pp_print_string ppf "I"

  let pp_state ppf = function
    | A -> Format.pp_print_string ppf "A"
    | B -> Format.pp_print_string ppf "B"
    | C -> Format.pp_print_string ppf "C"
    | D -> Format.pp_print_string ppf "D"
    | Halt -> Format.pp_print_string ppf "Halt"
     
end

module TuringM = TuringMachine.Make(M)

open M 

let final_machine =
  TuringM.create
    ~init_state:A
    ~tape:(Tape.create (Colist.const O) O (Colist.const O))
  |> TuringM.run ~iter:(Format.printf "%a@.@." (TuringM.pp ~length:10))
