type 'elt t

val create : 'elt Colist.t -> 'elt -> 'elt Colist.t -> 'elt t

val move_left : 'elt t -> 'elt t

val move_right : 'elt t -> 'elt t

val move : [ `Left | `Right | `Stay ] -> 'elt t -> 'elt t 

val set : 'elt -> 'elt t -> 'elt t

val get : 'elt t -> 'elt 

val pp :
  ?length:int ->
  (Format.formatter -> 'elt -> unit) ->
  Format.formatter -> 'elt t -> unit
