module M =
struct
  type bit = O | I 
  type symbol =
    | Pair of bit * bit
    | Single of bit
    | Empty

  type state =
    | Carry
    | NoCarry

  let transition state symbol =
    match state, symbol with
    | Carry, Pair (O,O) -> Some (Single I,`Right,NoCarry)
    | Carry, Pair (I,O) -> Some (Single O,`Right,Carry)
    | Carry, Pair (O,I) -> Some (Single O,`Right,Carry)
    | Carry, Pair (I,I) -> Some (Single I,`Right,Carry)
    | NoCarry, Pair (O,O) -> Some (Single O,`Right,NoCarry)
    | NoCarry, Pair (I,O) -> Some (Single I,`Right,NoCarry)
    | NoCarry, Pair (O,I) -> Some (Single I,`Right,NoCarry)
    | NoCarry, Pair (I,I) -> Some (Single O,`Right,Carry)
    | Carry, Empty -> Some (Single I, `Right, NoCarry)
    | NoCarry, Empty -> None
    | unexpected -> None
  
  
  let pp_bit ppf = function
    | O -> Format.pp_print_string ppf "0"
    | I -> Format.pp_print_string ppf "1"
  
  let pp_symbol ppf = function
    | Pair (bit0,bit1) ->
      Format.fprintf ppf "%a+%a" pp_bit bit0 pp_bit bit1
    | Single bit ->
      Format.fprintf ppf "%a" pp_bit bit
    | Empty -> Format.fprintf ppf " "

  let pp_state ppf = function
    | Carry -> Format.pp_print_string ppf "Carry"
    | NoCarry -> Format.pp_print_string ppf "No carry"
  
end

open M
    
let to_bit = function
  | true -> I
  | false -> O 

let get_parity (n,p) =
  if n = 0 && p = 0 then ((n,p), Empty)
  else ((n/2, p/2), Pair (to_bit (n mod 2 = 0), to_bit (p mod 2 = 0)))


let initial_tape n p =
  Tape.create
    (Colist.const Empty)
    Empty
    (Colist.unfold (n,p) ~f:get_parity)
  |> Tape.move_right     


module TuringM = TuringMachine.Make(M)


let final_machine =
  TuringM.create
    ~init_state:NoCarry
    ~tape:(initial_tape 42 137)
  |> TuringM.run
    ~iter:(fun machine -> Format.printf "%a@.@." (TuringM.pp ~length:10) machine)
    

