type 'elt cell = Cons of ('elt * 'elt t)
and 'elt t = 'elt cell Lazy.t 

let rec const c =
  lazy (Cons (c, const c))

let rec iterate init ~f =
  lazy (Cons (init, iterate (f init) f))

let rec unfold init_state ~f =
  lazy (Cons (
      let (next_state, value) = f init_state in 
      (value, unfold ~f next_state)
    ))
    
    
let observe (lazy (Cons (head,tail))) =
  (head, tail)


let append elt colist =
  lazy (Cons (elt, colist))

let rec take i (lazy (Cons (head,tail))) =
  if i = 0 then []
  else head :: take (i-1) tail

let rec drop i ((lazy (Cons (head,tail))) as colist) =
  if i = 0 then colist
  else drop (i-1) tail

let rec zip (lazy (Cons (left,left_tail))) (lazy (Cons (right, right_tail))) =
  lazy (Cons (
      (left,right),
      zip left_tail right_tail
    ))



let rec map (lazy (Cons (head,tail))) ~f =
  lazy (Cons (f head, map tail ~f))


let rec filter (lazy (Cons (head, tail))) ~f =
  lazy (
    if f head then 
      Cons (head, filter tail ~f)
    else
      Lazy.force (filter tail ~f)
  )


let rec scan ~f ~init (lazy (Cons (head, tail))) =
  lazy (Cons (
      init,
      scan ~f ~init:(f init head) tail
    ))
   
