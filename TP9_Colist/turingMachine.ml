type move = [ `Left | `Right | `Stay ]

module type S = sig
  type symbol
  type state
  
  val pp_symbol : Format.formatter -> symbol -> unit
  val pp_state : Format.formatter -> state -> unit

  val transition : state -> symbol -> (symbol * move * state) option 
end



module Make = functor (Machine : S) ->
struct

  type machine =
    { state : Machine.state;
      tape : Machine.symbol Tape.t;
      position : int;
      timestep : int;
    }

  let create ~init_state ~tape =
    { state = init_state;
      tape;
      position = 0;
      timestep = 0
    }

  let get_state { state } = state
  let get_tape { tape } = tape
  let get_position { position } = position
  let get_timestep { timestep } = timestep

  let update_position displacement position =
    match displacement with
    | `Left -> position - 1
    | `Right -> position + 1
    | `Stay -> position
  
  let perform_transition machine =
    match Machine.transition machine.state (Tape.get machine.tape) with
    | None -> None
    | Some (symbol, displacement, state) ->
      Some {
        state;
        tape = machine.tape |> Tape.set symbol |> Tape.move displacement;
        position = update_position displacement machine.position;
        timestep = machine.timestep + 1
      }

  let rec run ?(iter = fun _ -> ()) machine =
    iter machine;
    match perform_transition machine with
    | None -> machine
    | Some new_machine -> run ~iter new_machine
    
  let pp ?(length=10) ppf machine =
    Format.fprintf ppf
      "@[<v 0>\
         @[State: %a@]@;\
         @[Step: %d@]@;\
         @[Position: %d@]@;\
         @[Tape: @[<hov 2>%a@]@]\
       @]"
      Machine.pp_state machine.state
      machine.timestep
      machine.position
      (Tape.pp ~length Machine.pp_symbol) machine.tape
  

end
