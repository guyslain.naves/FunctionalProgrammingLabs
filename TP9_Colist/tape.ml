type 'elt t =
  { left : 'elt Colist.t;
    head : 'elt;
    right : 'elt Colist.t 
  }

let create left head right = { left; head; right }


let move_left { left; head; right } =
  let (new_head, new_left) = Colist.observe left in
  { left = new_left;
    head = new_head;
    right = Colist.append head right
  }

let move_right { left; head; right } =
  let (new_head, new_right) = Colist.observe right in
  { left = Colist.append head left;
    head = new_head;
    right = new_right
  }

let move displacement tape =
  match displacement with
  | `Left -> move_left tape
  | `Right -> move_right tape
  | `Stay -> tape


let set head tape = { tape with head }

let get tape = tape.head

(* Format.pp_print_list since OCaml 4.02 *)
let format_pp_print_list ?(pp_sep=Format.pp_print_cut) pp_elt ppf list =
  let rec loop ppf = function
    | [] -> ()
    | [single] -> pp_elt ppf single
    | first::others ->
      Format.fprintf ppf "%a%a%a"
        pp_elt first
        pp_sep ()
        loop others
  in
  loop ppf list 

let pp ?(length=10) pp_elt ppf tape =
  let prefix = Colist.take length tape.left |> List.rev in
  let suffix = Colist.take length tape.right in
  let pp_sides = 
    format_pp_print_list
      ~pp_sep:(fun ppf () -> Format.fprintf ppf "@ | ")
      pp_elt
  in
  Format.fprintf ppf "%a >>@ %a@ << %a"
    pp_sides prefix
    pp_elt tape.head
    pp_sides suffix
    
    
