type move = [ `Left | `Right | `Stay ]

module type S = sig
  type symbol
  type state
  
  val pp_symbol : Format.formatter -> symbol -> unit
  val pp_state : Format.formatter -> state -> unit

  val transition : state -> symbol -> (symbol * move * state) option 
end

module Make : functor (Machine : S) ->
sig
  type machine

  val create : init_state:Machine.state -> tape:Machine.symbol Tape.t -> machine

  val get_state : machine -> Machine.state
  val get_tape : machine -> Machine.symbol Tape.t
  val get_position : machine -> int
  val get_timestep : machine -> int

  val perform_transition : machine -> machine option

  val run : ?iter:(machine -> unit) -> machine -> machine

  val pp : ?length:int -> Format.formatter -> machine -> unit 
  
end
