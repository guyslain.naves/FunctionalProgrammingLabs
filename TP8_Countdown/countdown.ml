type op = Plus | Minus | Prod | Div
let operators = [Plus; Minus; Prod; Div]

let fun_of_op = function
  | Plus -> (+)
  | Minus -> (-)
  | Prod -> ( * )
  | Div -> (/)

let score_of_op = function
  | Plus -> 20
  | Minus -> 40
  | Prod -> 50
  | Div -> 100

type expr =
  | Literal of int
  | Node of expr * op * expr

let rec eval = function
  | Literal i -> i
  | Node (a,op,b) -> fun_of_op op (eval a) (eval b)

let rec score = function
  | Literal i -> i
  | Node (a,op,b) -> score a + score_of_op op + score b


let not_op op = function
  | Node (_,op2,_) when op = op2 -> false
  | _ -> true

let valid_combination op left right =
  match op with
  | Plus ->
    (not_op Plus right) && (eval left >= eval right)
  | Minus ->
    (not_op Minus right) && (not_op Minus left) && (eval left > eval right)
  | Prod ->
    (eval left > 1)
    && (eval right > 1)
    && (not_op Prod right)
    && (eval left >= eval right)
  | Div ->
    (not_op Div left) && (not_op Div right) 
    && (eval right > 1)
    && (eval left) mod (eval right) = 0

let combine left operator right =
  if valid_combination operator left right
  then [Node (left,operator,right)]
  else []

let (>>=) list fct = list |> List.map fct |> List.concat 



let rec bipartitions = function
  | [] -> [[],[]]
  | head::tail ->
    bipartitions tail >>= fun (left,right) ->
    [ (head::left,right); (left,head::right) ] 

let rec fold_partitions f list value =
  match list with
  | [] -> f [] [] value
  | head::tail -> 
    fold_partitions 
      (fun part1 part2 value -> f part1 (head::part2) (f (head::part1) part2 value)) 
      tail value


module Int = 
struct
  type t = int
  let compare a b = a - b
end
module IMap = Map.Make(Int)


let record_expression map expr =
  let value = eval expr in 
  if IMap.mem value map && (score (IMap.find value map) <= score expr) 
  then map
  else IMap.add value expr map
  

let fold_map_pairs f map1 map2 value =
  IMap.fold 
    (fun _ bound1 value ->
       IMap.fold
         (fun _ bound2 value -> f bound1 bound2 value)
         map2
         value
    )
    map1
    value

let countdown0 countdown = function
  | [] -> IMap.empty
  | [single] -> IMap.add single (Literal single) IMap.empty
  | int_list ->
    let possible_expressions =
      bipartitions int_list >>= fun (left,right) ->
      if left = [] || right = [] then [] else
      (countdown left |> IMap.bindings) >>= fun (left_value, left_expr) ->
      (countdown right |> IMap.bindings) >>= fun (right_value, right_expr) ->
      operators >>= fun operator ->
      combine left_expr operator right_expr
    in
    List.fold_left record_expression IMap.empty possible_expressions

let rec countdown_naive int_list = countdown0 countdown_naive int_list

module IList =
struct
  type t = int list
  let rec compare l1 l2 = match l1, l2 with
    | [], [] -> 0
    | [], _ -> -1
    | _, [] -> 1
    | head1::_, head2::_ when head1 <> head2 -> head1 - head2
    | _::tail1, _::tail2 -> compare tail1 tail2
end
module ILMap = Map.Make(IList)

let rec sublists = function 
  | [] -> [[]]
  | head::tail ->
    sublists tail >>= fun subtail ->
    [ subtail; head::subtail ]

let fib0 fib = function
  | 0 
  | 1 -> 1
  | n ->  fib (n-1) + fib (n-2)

let rec fib_naive i = fib0 fib_naive i

let rec interval a b = if a > b then [] else a::(interval (a+1) b)

let better_fib n =
  let map =
    List.fold_left
      (fun map i -> IMap.add i (fib0 (fun j -> IMap.find j map) i) map)
      IMap.empty
      (interval 0 n)
  in IMap.find n map


module Memo = functor (M : Map.S) ->
struct
  let memoize enumeration induction =
    let map = 
      List.fold_left
        (fun map sub_instance ->
          M.add 
            sub_instance 
            (induction (fun sub_sub_instance -> M.find sub_sub_instance map) sub_instance)
            map
        )
        M.empty
        enumeration
    in
    (fun instance -> M.find instance map)
end

module IMemo = Memo(IMap)

let fib_dp n = IMemo.memoize (interval 0 n) fib0 n

module ILMemo = Memo(ILMap)

let countdown_dp multiset = ILMemo.memoize (sublists multiset) countdown0 multiset


(* *** simple test *** *)
(* let solutions =
 *   countdown_dp [4;5;7;8;9;10]
 * 
 * let _ =
 *   for i = 100 to 999 do
 *     if not (IMap.mem i solutions) then Printf.printf "%d\n" i
 *   done *)
    
(* *** *)


let priority_of_op = function 
  | Plus | Minus -> 1
  | Prod | Div -> 2

let associativity_of_op = function
  | Plus | Prod -> true
  | Minus | Div -> false

let string_of_op = function
  | Plus -> "+"
  | Minus -> "-"
  | Prod -> "*"
  | Div -> "/"

let rec string_of_expr prior assoc = function
  | Literal i -> Printf.sprintf "%i" i
  | Node (left,op,right) ->
    begin
      let priority = priority_of_op op in
      let associativity = associativity_of_op op in
      let string_left = string_of_expr priority associativity left in
      let string_right = string_of_expr priority associativity right in
      if (priority < prior) || (priority = prior && not assoc)
      then Printf.sprintf "(%s %s %s)" string_left (string_of_op op) string_right
      else Printf.sprintf "%s %s %s" string_left (string_of_op op) string_right
    end

let str_of_expr expr = string_of_expr 0 true expr


let choice_list = (interval 1 10) @ (interval 1 10) @ [25;50;75;100]

let _ = Random.self_init ()

let random_choice list =
  let i = Random.int (List.length list) in
  let rec remove prefix index = function
    | [] -> assert false
    | head::suffix -> 
      if index = 0 then head,(List.rev_append prefix suffix)
      else remove (head::prefix) (index-1) suffix
  in remove [] i list

let random_instance () =
  let (multiset,_) =
    List.fold_left 
      (fun (selected,remains) _ ->
         let (i,remains) = random_choice remains in (i::selected,remains))
      ([],choice_list) (interval 1 6)
  in
  (List.fast_sort compare multiset, 100 + Random.int 900) 
  
let nearest imap goal =
  if IMap.mem goal imap then goal
  else
    IMap.fold
      (fun j _ best -> if abs (goal - j) < abs (goal - best) then j else best)
      imap
      0

let main =
  let to_int string =
    try [int_of_string string] 
    with _ -> []
  in
  let print_expr goal expr = Printf.printf "%i = %s\n" goal (str_of_expr expr) in
  let int_args = List.flatten (List.map to_int (List.tl (Array.to_list Sys.argv))) in
  let array_exists p = Array.fold_left (fun b elt -> b || p elt) false in

  if array_exists (fun str -> str = "-a") Sys.argv then
    IMap.iter print_expr (countdown_dp (List.fast_sort compare int_args)) 
  else if array_exists (fun str -> str = "-q") Sys.argv then
    begin
      let (multiset,goal) = random_instance () in
      let solutions = countdown_dp multiset in
      Printf.printf "Try to find %i using only: [%s]\n" goal
        (String.concat "; " (List.map string_of_int multiset));
      flush stdout;
      Unix.sleep 30;
      let nearest_value = nearest solutions goal in
      Printf.printf "Best solution found:\n";
      print_expr nearest_value (IMap.find nearest_value solutions)
    end
  else
    match int_args with
      | goal::multiset ->
        let solutions = countdown_dp (List.fast_sort compare multiset) in
        if IMap.mem goal solutions then print_expr goal (IMap.find goal solutions)
        else  Printf.printf "No solutions.\n"   
      | _ -> Printf.printf "Invalid number of arguments: \
                          I expect the goal, then a multisets of integer.\n"
  
